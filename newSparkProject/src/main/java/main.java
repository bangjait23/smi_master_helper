import static spark.Spark.*;

import Category.*;
import Comment.CommentModel;
import DataUser.DataUserModel;
import DataUser.userTicketModel;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class main {
    public static Map users = new HashMap<>();
    private static UserSevice userSevice = new UserSevice();
    private static Test test = new Test();
    private static ObjectMapper om = new ObjectMapper();
    public  static  void main(String[] args){
        ResponseModel respons = new ResponseModel();
        port(9000);
        get("/", (request, response) -> "welcome");

        //AddUser
        post("/user/add",((request, response) -> {
            String id_user = request.queryParams("id_user");
            String id_priority = request.queryParams("id_priority");
            String id_category = request.queryParams("id_category");
            String id_subcategory = request.queryParams("id_SubCategory");
            String name_branch = request.queryParams("name_branch");
            String id_directorate = request.queryParams("id_directorate");
            String subject = request.queryParams("subject");
            String phoneDept = request.queryParams("phoneDept");
            String phone = request.queryParams("phone");
            String desc= request.queryParams("description");
            String email1 = request.queryParams("email_1");
            String email2 = request.queryParams("email_2");
            String email3 = request.queryParams("email_3");
            String email4 = request.queryParams("email_4");
            String email5 = request.queryParams("email_5");
            String status = "1";
            String tanggal = request.queryParams("tanggal");
            String has_attachment = request.queryParams("has_attachment");
            int id = userSevice.getMaxID()+1;
            System.out.println("ID Max : "+id);
            User user = userSevice.add(id, id_user, id_priority, id_category, id_subcategory, name_branch, id_directorate, subject,
                    phoneDept, phone, desc, email1, email2, email3, email4, email5, tanggal, status, has_attachment);

            respons.setResponseMessage("success");
            return om.writeValueAsString(respons);
        }));
        get("/user/:id", ((request, response) -> {
            User user = userSevice.findById(request.params(":id"));
            if(user!=null){
                return om.writeValueAsString(user);
            }
            else{
                response.status(404);

                return om.writeValueAsString("User Not Found");
            }
        }));
//        get("/user",((request, response) -> {
//            List result = userSevice.findAll();
//            if (result.isEmpty()) {
//                return om.writeValueAsString("user not found");
//            } else {
//                return om.writeValueAsString(userSevice.findAll());
//            }
//        }));
        get("/ticket/:id_user",((request, response) -> {
            ResponseModel responseModel = new ResponseModel();
            ArrayList<userTicketModel> tickets = userSevice.findTicketUser(request.params(":id_user"));
            if(tickets!=null){
                return om.writeValueAsString(tickets);
            }
            else{
                responseModel.setResponseCode("404");
                responseModel.setResponseMessage("user not found");
                response.status(404);
                return om.writeValueAsString(responseModel);
            }
        }));
        //search ticket
        get("search/ticket/:id_ticket",((request, response) -> {
            ResponseModel responseModel = new ResponseModel();
            ArrayList<userTicketModel> tickets = (ArrayList<userTicketModel>) userSevice.SearchTicketID(request.params(":id_ticket"));
            if(tickets!=null){
                return om.writeValueAsString(tickets);
            }
            else{
                responseModel.setResponseCode("404");
                responseModel.setResponseMessage("user not found");
                response.status(404);
                return om.writeValueAsString(responseModel);
            }
        }));

        put("update/ticket/:id_ticket", (request, response) -> {
            ResponseModel responseModel = new ResponseModel();
            String id = request.params(":id_ticket");
            User user = userSevice.findById(id);
            if (user != null) {
                String status = request.queryParams("status");
                userSevice.update(id, status);
                responseModel.setResponseCode("200");
                responseModel.setResponseMessage("user with id " + id + " is updated!");

                return om.writeValueAsString(responseModel);
            } else {
                responseModel.setResponseCode("404");
                responseModel.setResponseMessage("user not found");
                response.status(404);
                return om.writeValueAsString(responseModel);
            }
        });
        delete("/user/:id",((request, response) -> {
            String id = request.params(":id");
            User user = userSevice.findById(id);
//            if (user != null) {
//                userSevice.delete(id);
//                return om.writeValueAsString("user with id " + id + " is deleted!");
//            } else {
//                response.status(404);
//                return om.writeValueAsString("user not found");
//            }
            try {
                Class.forName("com.mysql.cj.jdbc.Driver");
                Connection conn = DriverManager.getConnection("JDBC:mysql://localhost:3306/mnc_database","root", "");
                String sql = "DELETE FROM tickets WHERE id = "+id;
                PreparedStatement ps = conn.prepareStatement(sql);
                ps.executeUpdate();
                ps.close();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return "sukses";
        }));



        //AddComment
        post("/comment/add", ((request, response) -> {
            String id_ticket = request.queryParams("id_ticket");
            String tanggapan = request.queryParams("tanggapan");
            String id_user = request.queryParams("id_user");

            System.out.println("id_ticket : "+id_ticket+"tanggapan : "+tanggapan+"id_user : "+id_user);
            CommentModel commentModel = userSevice.AddComment(id_ticket, tanggapan, id_user);

            Class.forName("oracle.jdbc.driver.OracleDriver");
            Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@192.168.177.70:1521:mncsvrpt", "IDV_REPORT", "idv_report");
            String sql = "update SMI_TICKET_APP_SUPPORT set status = '3' where id = "+id_ticket;
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.executeUpdate();
            ps.close();

            return om.writeValueAsString(commentModel);
        }));

        //Get Comment Ticket
        get("/comment/:id_ticket", ((request, response) -> {
            ArrayList<CommentModel> commentModel = userSevice.findCommentId(request.params(":id_ticket"));
            if(commentModel!=null){
                return om.writeValueAsString(commentModel);
            }
            else{
                response.status(404);
                return om.writeValueAsString("User Not Found");
            }
        }));

        //Login User
        get("/user/:email/:password", (request, response) -> {
            ResponseModel responseModel = new ResponseModel();
            String username = request.params(":email");
            String pasword = request.params(":password");
            DataUserModel dataUserModel = userSevice.findUser(username, pasword);
//            System.out.println(dataUserModel.getUsername());
//            ArrayList<DataUserModel> userModel = new ArrayList<DataUserModel>();

            if(dataUserModel != null){

                return om.writeValueAsString(dataUserModel);
            }
            else {
                responseModel.setResponseCode("404");
                responseModel.setResponseMessage("Account not registered");
                response.status(404);

                return om.writeValueAsString(responseModel);
            }
        });


        //Change PasswordUser
        put("/user/password/:id", (request, response) -> {
            ResponseModel responseModel = new ResponseModel();
            String id = request.params(":id");
            DataUserModel dataUserModel = userSevice.findByIdUser(id);
            if (dataUserModel != null) {
                String password = request.queryParams("password");
                userSevice.changePassword(id, password);
                responseModel.setResponseCode("1");
                responseModel.setResponseMessage("Update Password Success");
                return om.writeValueAsString(responseModel);
            } else {
                responseModel.setResponseCode("404");
                responseModel.setResponseMessage("user not found");
                response.status(404);
                return om.writeValueAsString(responseModel);
            }
        });

        //get Master Category
        get("/Category/All",((request, response) -> {
            ResponseModel responseModel = new ResponseModel();
            ArrayList<CategoryModel> categoryModel = userSevice.findAllCategory();
            //System.out.println(dataUserModel.getUsername());
            //ArrayList<DataUserModel> userModel = new ArrayList<DataUserModel>();
            if(categoryModel != null){

                return om.writeValueAsString(categoryModel);
            }
            else {
                responseModel.setResponseCode("404");
                responseModel.setResponseMessage("Category not registered");
                response.status(404);

                return om.writeValueAsString(responseModel);
            }
        }));
        //Get master type SFA OR APP
        get("/Type",((request, response) -> {
            ResponseModel responseModel = new ResponseModel();
            ArrayList<Type> type = userSevice.findType();
            //System.out.println(dataUserModel.getUsername());
            //ArrayList<DataUserModel> userModel = new ArrayList<DataUserModel>();
            if(type != null){

                return om.writeValueAsString(type);
            }
            else {
                responseModel.setResponseCode("404");
                responseModel.setResponseMessage("Type not registered");
                response.status(404);

                return om.writeValueAsString(responseModel);
            }
        }));


        //Get master priority
        get("/Priority",((request, response) -> {
            ResponseModel responseModel = new ResponseModel();
            ArrayList<priority> priorities = userSevice.findPriority();
            //System.out.println(dataUserModel.getUsername());
            //ArrayList<DataUserModel> userModel = new ArrayList<DataUserModel>();
            if(priorities != null){

                return om.writeValueAsString(priorities);
            }
            else {
                responseModel.setResponseCode("404");
                responseModel.setResponseMessage("Type not registered");
                response.status(404);

                return om.writeValueAsString(responseModel);
            }
        }));


        //Get master Branch
        get("/Branch",((request, response) -> {
            ResponseModel responseModel = new ResponseModel();
            ArrayList<Branch> branches = userSevice.findBranch();
            //System.out.println(dataUserModel.getUsername());
            //ArrayList<DataUserModel> userModel = new ArrayList<DataUserModel>();
            if(branches != null){

                return om.writeValueAsString(branches);
            }
            else {
                responseModel.setResponseCode("404");
                responseModel.setResponseMessage("Type not registered");
                response.status(404);

                return om.writeValueAsString(responseModel);
            }
        }));

        //Get Directorate Master
        get("/Directorate",((request, response) -> {
            ResponseModel responseModel = new ResponseModel();
            ArrayList<DirectorateModel> directorateModels = userSevice.findDirectorate();
            //System.out.println(dataUserModel.getUsername());
            //ArrayList<DataUserModel> userModel = new ArrayList<DataUserModel>();
            if(directorateModels != null){

                return om.writeValueAsString(directorateModels);
            }
            else {
                responseModel.setResponseCode("404");
                responseModel.setResponseMessage("Type not registered");
                response.status(404);

                return om.writeValueAsString(responseModel);
            }
        }));


        //Get Sub Category Master
        get("/Category/:parent_id/SubCategory",((request, response) -> {
            ResponseModel responseModel = new ResponseModel();
            System.out.println("ID Parent : "+request.params(":parent_id"));
            ArrayList<SubCategory> subCategories = userSevice.findSubCategory(request.params(":parent_id"));
            //System.out.println(dataUserModel.getUsername());
            //ArrayList<DataUserModel> userModel = new ArrayList<DataUserModel>();
            if(subCategories != null){

                return om.writeValueAsString(subCategories);
            }
            else {
                responseModel.setResponseCode("404");
                responseModel.setResponseMessage("SubCategory Not Found not registered");
                response.status(404);

                return om.writeValueAsString(responseModel);
            }
        }));

        //test post controller
        post("/test/post", ((request, response) -> {
            int id = Integer.parseInt(request.queryParams("id"));
            String username = request.queryParams("username");
            String password = request.queryParams("password");
            String name = request.queryParams("name");
            String email = request.queryParams("email");
            int status_id = Integer.parseInt(request.queryParams("status_id"));
            DataUserModel commentModel = test.cobaPost(id, username, password, name, email, status_id);
            return om.writeValueAsString(commentModel);
        }));
    }
}
