package Comment;

public class CommentModel {
    private String id_ticket;
    private String tanggapan;
    private String id_user;
    private String name;

    public CommentModel(String id_ticket, String tanggapan, String id_user) {
        this.id_ticket = id_ticket;
        this.tanggapan = tanggapan;
        this.id_user = id_user;
    }

    public CommentModel(String id_ticket, String tanggapan, String id_user, String name) {
        this.id_ticket = id_ticket;
        this.tanggapan = tanggapan;
        this.id_user = id_user;
        this.name = name;
    }

    public String getId_ticket() {
        return id_ticket;
    }

    public void setId_ticket(String id_ticket) {
        this.id_ticket = id_ticket;
    }

    public String getTanggapan() {
        return tanggapan;
    }

    public void setTanggapan(String tanggapan) {
        this.tanggapan = tanggapan;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "CommentModel{" +
                "id_ticket='" + id_ticket + '\'' +
                ", tanggapan='" + tanggapan + '\'' +
                ", id_user='" + id_user + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
