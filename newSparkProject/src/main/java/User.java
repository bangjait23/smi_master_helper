public class User {

    private int id;
    String id_user;
    String id_priority;
    String id_category;
    String id_subcategory;
    String name_branch;
    String id_directorate;
    String subject;
    String phoneDept;
    String phone;
    String desc;
    String email1, email2, email3, email4, email5;
    String tanggal;
    String status;
    String has_attachment;

    public User() {
    }

    public User(int id) {
        this.id = id;
    }
    public User(int id, String id_user, String id_priority, String id_category, String id_subcategory, String name_branch, String id_directorate, String subject, String phoneDept, String phone, String desc, String email1, String email2, String email3, String email4, String email5, String tanggal, String status, String has_attachment) {
        this.id = id;
        this.id_user = id_user;
        this.id_priority = id_priority;
        this.id_category = id_category;
        this.id_subcategory = id_subcategory;
        this.name_branch = name_branch;
        this.id_directorate = id_directorate;
        this.subject = subject;
        this.phoneDept = phoneDept;
        this.phone = phone;
        this.desc = desc;
        this.email1 = email1;
        this.email2 = email2;
        this.email3 = email3;
        this.email4 = email4;
        this.email5 = email5;
        this.tanggal = tanggal;
        this.status = status;
        this.has_attachment = has_attachment;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getId_priority() {
        return id_priority;
    }

    public void setId_priority(String id_priority) {
        this.id_priority = id_priority;
    }

    public String getId_category() {
        return id_category;
    }

    public void setId_category(String id_category) {
        this.id_category = id_category;
    }

    public String getId_subcategory() {
        return id_subcategory;
    }

    public void setId_subcategory(String id_subcategory) {
        this.id_subcategory = id_subcategory;
    }

    public String getName_branch() {
        return name_branch;
    }

    public void setName_branch(String name_branch) {
        this.name_branch = name_branch;
    }

    public String getId_directorate() {
        return id_directorate;
    }

    public void setId_directorate(String id_directorate) {
        this.id_directorate = id_directorate;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getPhoneDept() {
        return phoneDept;
    }

    public void setPhoneDept(String phoneDept) {
        this.phoneDept = phoneDept;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getEmail1() {
        return email1;
    }

    public void setEmail1(String email1) {
        this.email1 = email1;
    }

    public String getEmail2() {
        return email2;
    }

    public void setEmail2(String email2) {
        this.email2 = email2;
    }

    public String getEmail3() {
        return email3;
    }

    public void setEmail3(String email3) {
        this.email3 = email3;
    }

    public String getEmail4() {
        return email4;
    }

    public void setEmail4(String email4) {
        this.email4 = email4;
    }

    public String getEmail5() {
        return email5;
    }

    public void setEmail5(String email5) {
        this.email5 = email5;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getHas_attachment() {
        return has_attachment;
    }

    public void setHas_attachment(String has_attachment) {
        this.has_attachment = has_attachment;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", id_user='" + id_user + '\'' +
                ", id_priority='" + id_priority + '\'' +
                ", id_category='" + id_category + '\'' +
                ", id_subcategory='" + id_subcategory + '\'' +
                ", name_branch='" + name_branch + '\'' +
                ", id_directorate='" + id_directorate + '\'' +
                ", subject='" + subject + '\'' +
                ", phoneDept='" + phoneDept + '\'' +
                ", phone='" + phone + '\'' +
                ", desc='" + desc + '\'' +
                ", email1='" + email1 + '\'' +
                ", email2='" + email2 + '\'' +
                ", email3='" + email3 + '\'' +
                ", email4='" + email4 + '\'' +
                ", email5='" + email5 + '\'' +
                ", tanggal='" + tanggal + '\'' +
                ", status='" + status + '\'' +
                ", has_attachment='" + has_attachment + '\'' +
                '}';
    }
}
