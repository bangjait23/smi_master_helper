import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectionClass {
    public static Connection conn;
    public static Statement statement;
    public static void getConnection(){
        try {
            String url = "JDBC:oracle:thin:@192.168.177.70:1521:mncsvrpt";
            String username = "idv_report";
            String password = "idv_report";
            Class.forName("oracle.jdbc.driver.OracleDriver");
            conn = DriverManager.getConnection(url, username, password);
            statement = conn.createStatement();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
