import Category.*;
import Comment.CommentModel;
import DataUser.DataUserModel;
import DataUser.userTicketModel;
import org.json.JSONArray;
import sun.misc.BASE64Decoder;
// import com.mysql.cj.api.mysqla.result.Resultset;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class UserSevice {
    public static Map users = new HashMap<>();
    public String nama;
    private static final AtomicInteger count = new AtomicInteger(0);
    //get Max ID Ticket
    public int getMaxID(){
        ResultSet resultSet;
        User uers = null;
        int idMAx = 0;
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@192.168.177.70:1521:mncsvrpt", "IDV_REPORT", "idv_report");
            String sql = "select max(id) as id from smi_ticket_app_support";
           Statement statement = conn.createStatement();
           resultSet = statement.executeQuery(sql);
            if(resultSet.next()){
                idMAx = resultSet.getInt("id");

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return idMAx;
    }


    public ArrayList<userTicketModel> findTicketUser(String id_user){
        ResultSet res;
        ArrayList<userTicketModel> tickets = new ArrayList<userTicketModel>();
            try{
                Class.forName("oracle.jdbc.driver.OracleDriver");
                Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@192.168.177.70:1521:mncsvrpt", "IDV_REPORT", "idv_report");
                String sql = "SELECT t.id as ticket_number, t.subject, t.phone_no, t.user_ext, t.message, t.cp_email1, t.cp_email2, t.cp_email3, t.cp_email4, t.cp_email5, t.date_created, t.dept_branch, " +
                        "c.category, st.status " +
                        " FROM smi_ticket_app_support t INNER JOIN smi_ticket_category c ON t.ticket_category = c.id" +
                        " INNER JOIN SMI_TICKET_STATUS st ON t.status = st.id " +
                        "WHERE t.user_id = "+id_user+" ORDER BY t.id DESC";
                PreparedStatement ps = conn.prepareStatement(sql);
                res = ps.executeQuery();
                while(res.next()){
                    userTicketModel uer = new userTicketModel(res.getString("ticket_number"),
                            res.getString("subject"), res.getString("phone_no"), res.getString("user_ext"),
                            res.getString("message"), res.getString("cp_email1"), res.getString("cp_email2"),
                            res.getString("cp_email3"), res.getString("cp_email4"), res.getString("cp_email5"),
                            res.getString("date_created"), res.getString("dept_branch"), res.getString("category"),
                            res.getString("status"));
                            ;
                    tickets.add(uer);
                }
                ps.close();
            } catch (Exception e) {
                System.out.println("Error Na : " + e.getMessage());
                e.printStackTrace();
            }
            return tickets;
    }


    public DataUserModel findUser(String username, String password){
        ResultSet res = null;
        Connection conn = null;
        Statement statement;
//        String sql = "SELECT id, username, password, name, email, status_id FROM am_user_mobile WHERE username = ?  AND password = ? ";
        DataUserModel dataUserModel = null;
        try{
//            Class.forName("com.mysql.cj.jdbc.Driver");
//            Connection conn = DriverManager.getConnection("JDBC:mysql://localhost:3306/mnc_database", "root", "");
            Class.forName("oracle.jdbc.driver.OracleDriver");
            conn = DriverManager.getConnection("jdbc:oracle:thin:@192.168.177.70:1521:mncsvrpt", "IDV_REPORT", "idv_report");
            statement = conn.createStatement();

//            System.out.println(sql);
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] hashInBytes = md.digest(password.getBytes(StandardCharsets.UTF_8));

            StringBuilder sb = new StringBuilder();
            for (byte b : hashInBytes) {
                sb.append(String.format("%02x", b));
            }
            res = statement.executeQuery("SELECT * FROM am_users WHERE username LIKE '"+username+"'  AND password LIKE'"+sb.toString()+"'");

          try {
              while(res.next()){
                  String name = res.getString("name");
                  String uname = res.getString("username");
                  String pwd = res.getString("password");
                  String email = res.getString("email");
                  String id = res.getString("id");
                  String status_id = res.getString("status_id");
                  dataUserModel = new DataUserModel(uname, pwd, name,email,id, status_id);

              }
          }
          catch (Exception e){
              System.out.println("Error na : "+e.getMessage());
          }
            statement.close();
            conn.close();
        } catch (Exception e) {
            System.out.println("Error Na : " + e.getMessage());
            e.printStackTrace();
        }
        return dataUserModel;
    }


    public User findById(String id) {
        ResultSet resultSet;
        User uer = null;
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@192.168.177.70:1521:mncsvrpt", "IDV_REPORT", "idv_report");
            String sql = "SELECT id FROM smi_ticket_app_support where id = "+id;
            PreparedStatement ps = conn.prepareStatement(sql);
            resultSet = ps.executeQuery();
            if(resultSet.next()){
                uer = new User(resultSet.getInt("id"));
            }
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return uer;
    }


    //Find User By ID
    public DataUserModel findByIdUser(String id){
        ResultSet resultSet;
        DataUserModel dataUserModel = null;
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@192.168.177.70:1521:mncsvrpt", "IDV_REPORT", "idv_report");
//            Class.forName("oracle.jdbc.driver.OracleDriver");
//            Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@192.168.177.70:1521:mncsvrpt", "IDV_REPORT", "idv_report");
            String sql = "SELECT id FROM user where id = "+id;
            PreparedStatement ps = conn.prepareStatement(sql);
            resultSet = ps.executeQuery();
            if(resultSet.next()){
                dataUserModel = new DataUserModel(resultSet.getString("id"));
            }
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return dataUserModel;
    }



        public User add(int id, String id_user, String id_priority, String id_category,
                        String id_subcategory, String id_branch, String id_directorate, String subject,
                        String phoneDept, String phone, String desc, String email1, String email2, String email3,
                        String email4, String email5, String tanggal, String status, String has_attachment){

            String statusGambar = null;
            User user = new User(id, id_user, id_priority, id_category, id_subcategory, id_branch, id_directorate, subject,
                phoneDept, phone, desc, email1, email2, email3, email4, email5, tanggal, status, has_attachment);

            //get ID MAX from database
            try{
            Class.forName("oracle.jdbc.driver.OracleDriver");
            Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@192.168.177.70:1521:mncsvrpt", "IDV_REPORT", "idv_report");
            String sql = "INSERT INTO smi_ticket_app_support (id, user_id, ticket_category, subject, message, dept_branch, user_ext, cp_email1, cp_email2, cp_email3, cp_email4, cp_email5, " +
                    "status, date_created, has_attachment, phone_no, priority_lvl, cat_lvl2, direktorat_id) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            ps.setInt(2, Integer.parseInt(id_user));
            ps.setInt(3, Integer.parseInt(id_category));
            ps.setString(4, subject);
            ps.setString(5, desc);
            ps.setString(6, id_branch);
            ps.setString(7, phone);
            ps.setString(8, email1);
            ps.setString(9, email2);
            ps.setString(10, email3);
            ps.setString(11, email4);
            ps.setString(12, email5);
            ps.setString(13, status);
            ps.setString(14, tanggal);
            ps.setString(15, has_attachment);
            ps.setString(16, phoneDept);
            ps.setInt(17, Integer.parseInt(id_priority));
            ps.setInt(18, Integer.parseInt(id_subcategory));
            ps.setInt(19, Integer.parseInt(id_directorate));
            ps.executeUpdate();
            ps.close();
        }catch (Exception e){
                System.out.println("Error na : "+e.getMessage());
        }
        return user;
    }
    public User update(String id, String status){
        User user = (User) users.get(id);
        try{
            Class.forName("oracle.jdbc.driver.OracleDriver");
            Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@192.168.177.70:1521:mncsvrpt", "IDV_REPORT", "idv_report");
            String sql = "UPDATE smi_ticket_app_support set status = ? WHERE id = "+id;
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, status);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return user;
    }
//    public List getTicket(String id, String subject, String dept, String phoneDept, String phone, String description, String email){
//        User ticket = (User) users.get(email);
//        ResultSet res;
//        ArrayList<User> tickets = new ArrayList<User>();
//
//        try{
//            Class.forName("com.mysql.cj.jdbc.Driver");
//            Connection conn = DriverManager.getConnection("JDBC:mysql://localhost:3306/mnc_database", "root", "");
//            String sql = "SELECT * FROM tickets where email like '%email%'";
//            PreparedStatement ps = conn.prepareStatement(sql);
//            res = ps.executeQuery();
//            while(res.next()){
//               ticket = new User(res.getInt("id"), res.getString("subject"),
//                        res.getString("category"), res.getString("dept"),
//                        res.getString("phoneDept"), res.getString("phone"),
//                        res.getString("description"), res.getString("email"));
//                tickets.add(ticket);
//            }
//            ps.close();
//        } catch (Exception e) {
//            System.out.println("Error Na : " + e.getMessage());
//            e.printStackTrace();
//        }
//        return tickets;
//    }
//    public void delete(String id){
//
//        try {
//            Class.forName("com.mysql.cj.jdbc.Driver");
//            Connection conn = DriverManager.getConnection("JDBC:mysql://localhost:3306/mnc_database","root", "");
//            String sql = "DELETE FROM tickets WHERE id = "+id;
//            PreparedStatement ps = conn.prepareStatement(sql);
//            ps.executeUpdate();
//            ps.close();
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//
//    }
    public List SearchTicketID(String id_ticket) throws SQLException {
        ResultSet res;
        ArrayList<userTicketModel> tickets = new ArrayList<userTicketModel>();
        try{
            Class.forName("oracle.jdbc.driver.OracleDriver");
            Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@192.168.177.70:1521:mncsvrpt", "IDV_REPORT", "idv_report");
            String sql = "SELECT t.id, t.subject, t.phoneDept, t.phone, t.description, t.email_1, t.email_2, t.email_3, t.email_4, t.email_5, t.dateTime," +
                    "c.category, b.org_name, d.directorate_name, s.sub_category, p.status, st.status_name " +
                    "FROM tickets t INNER JOIN category c ON t.id_category = c.id" +
                    " INNER JOIN branch b ON t.id_branch  = b.org_id" +
                    " INNER JOIN directorat d ON t.id_direktorat = d.id_direktorat" +
                    " INNER JOIN sub_category s ON t.id_subCategory = s.id" +
                    " INNER JOIN priority p ON t.id_priority = p.id_priority" +
                    " INNER JOIN STATUS st ON t.status = st.id_status WHERE t.id = "+id_ticket;
            PreparedStatement ps = conn.prepareStatement(sql);
            res = ps.executeQuery();
            while(res.next()){
//                userTicketModel uer = new userTicketModel(res.getString("id"), res.getString("id_user"), res.getString("name"), res.getString("subject"),
//                        res.getString("category"), res.getString("dept"),
//                        res.getString("phoneDept"), res.getString("phone"),
//                        res.getString("description"), res.getString("status"),res.getString("email"), res.getString("dateTime"));
//                tickets.add(uer);
            }
            ps.close();
        } catch (Exception e) {
            System.out.println("Error Na : " + e.getMessage());
            e.printStackTrace();
        }
        return tickets;
    }

    //REST API For Comment
    public CommentModel AddComment(String id_ticket, String tanggapan, String id_user){
        int currentId = count.incrementAndGet();
        CommentModel commentModel = new CommentModel(id_ticket, tanggapan, id_user);
        try{
            Class.forName("oracle.jdbc.driver.OracleDriver");
            Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@192.168.177.70:1521:mncsvrpt", "IDV_REPORT", "idv_report");
            String sql = "INSERT INTO SMI_TICKET_REPLY (ticket_id, message, userid) VALUES(?, ?, ?)";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, Integer.parseInt(id_ticket));
            ps.setString(2, tanggapan);
            ps.setInt(3, Integer.parseInt(id_user));
            ps.executeUpdate();
            ps.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        users.put(String.valueOf(currentId), commentModel);
        return commentModel;
    }

    //Get Comment For ticket
    public ArrayList<CommentModel> findCommentId(String id_ticket){
        ResultSet result;
        ArrayList<CommentModel> comments= new ArrayList<CommentModel>();
        try{
            Class.forName("oracle.jdbc.driver.OracleDriver");
            Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@192.168.177.70:1521:mncsvrpt", "IDV_REPORT", "idv_report");
            String sql = "SELECT b.ticket_id, b.message, b.userid, u.name " +
                    "                    FROM smi_ticket_reply b INNER JOIN am_users u ON b.userid = u.id WHERE b.ticket_id= "+id_ticket+" ORDER BY b.id ASC";
            PreparedStatement ps = conn.prepareStatement(sql);
            result = ps.executeQuery();
            while (result.next()){
                CommentModel commentModel = new CommentModel(result.getString("ticket_id"),
                        result.getString("message"), result.getString("userid"), result.getString("name"));
                comments.add(commentModel);
                System.out.println(comments);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return comments;
    }


    public DataUserModel changePassword(String id, String password){
        DataUserModel user = (DataUserModel) users.get(id);
        try{

            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] hashInBytes = md.digest(password.getBytes(StandardCharsets.UTF_8));

            StringBuilder sb = new StringBuilder();
            for (byte b : hashInBytes) {
                sb.append(String.format("%02x", b));
            }
            //Class.forName("com.mysql.cj.jdbc.Driver");
//            Class.forName("oracle.jdbc.driver.OracleDriver");
            Class.forName("oracle.jdbc.driver.OracleDriver");
            Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@192.168.177.70:1521:mncsvrpt", "IDV_REPORT", "idv_report");
//            Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@192.168.177.70:1521:mncsvrpt", "IDV_REPORT", "idv_report");
            String sql = "UPDATE user set password = '"+sb.toString()+"' WHERE id = "+id;
            PreparedStatement ps = conn.prepareStatement(sql);
            System.out.println("new Passsword : "+password);
            System.out.println("id : "+id);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return user;
    }
    //Get Category Master From Database
    public ArrayList<CategoryModel> findAllCategory(){
        ResultSet result;
        CategoryModel categoryModel;
        ArrayList<CategoryModel> category = new ArrayList<CategoryModel>();
        try{
            Class.forName("oracle.jdbc.driver.OracleDriver");
            Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@192.168.177.70:1521:mncsvrpt", "IDV_REPORT", "idv_report");
//            Class.forName("com.mysql.cj.jdbc.Driver");
//            Connection conn = DriverManager.getConnection("JDBC:mysql://localhost:3306/mnc_database","root", "");
            String sql = "SELECT * FROM smi_ticket_category";
            PreparedStatement ps = conn.prepareStatement(sql);
            result = ps.executeQuery();
            JSONArray json = new JSONArray();
            while (result.next()){
                categoryModel = new CategoryModel(result.getString("id"), result.getString("category"), result.getString("group_cat"));
                category.add(categoryModel);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return category;
    }

    //Get Type SFA or APP master from database
    public ArrayList<Type> findType(){
        ResultSet result;
        Type typeModel;
        ArrayList<Type> type= new ArrayList<Type>();
        try{
            Class.forName("oracle.jdbc.driver.OracleDriver");
            Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@192.168.177.70:1521:mncsvrpt", "IDV_REPORT", "idv_report");
//            Class.forName("com.mysql.cj.jdbc.Driver");
//            Connection conn = DriverManager.getConnection("JDBC:mysql://localhost:3306/mnc_database","root", "");
            String sql = "select * from SMI_TICKET_TYPE";
            PreparedStatement ps = conn.prepareStatement(sql);
            result = ps.executeQuery();
            while (result.next()){
               typeModel = new Type(result.getString("TICKET_TYPE_ID"), result.getString("TICKET_TYPE_DESC"));
               type.add(typeModel);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return type;
    }

    //Get Priority Master From Database
    public ArrayList<priority> findPriority(){
        ResultSet result;
        priority penting;
        ArrayList<priority> prioritiese= new ArrayList<priority>();
        try{
            Class.forName("oracle.jdbc.driver.OracleDriver");
            Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@192.168.177.70:1521:mncsvrpt", "IDV_REPORT", "idv_report");
//            Class.forName("com.mysql.cj.jdbc.Driver");
//            Connection conn = DriverManager.getConnection("JDBC:mysql://localhost:3306/mnc_database","root", "");
            String sql = "select * from SMI_M_TICKET_LEVEL";
            PreparedStatement ps = conn.prepareStatement(sql);
            result = ps.executeQuery();
            while (result.next()){
                penting = new priority(result.getString("level_id"), result.getString("level_desc"));
                prioritiese.add(penting);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return prioritiese;
    }

    //Get Branch Master from Database
    public ArrayList<Branch> findBranch(){
        ResultSet result;
        Branch branchModel;
        ArrayList<Branch> branches = new ArrayList<Branch>();
        try{
            Class.forName("oracle.jdbc.driver.OracleDriver");
            Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@192.168.177.70:1521:mncsvrpt", "IDV_REPORT", "idv_report");
//            Class.forName("com.mysql.cj.jdbc.Driver");
//            Connection conn = DriverManager.getConnection("JDBC:mysql://localhost:3306/mnc_database","root", "");
            String sql = "select * from Z_MASTER_AREA";
            PreparedStatement ps = conn.prepareStatement(sql);
            result = ps.executeQuery();
            while (result.next()){
                branchModel = new Branch(result.getString("prop_kode"), result.getString("org_id"), result.getString("org_name"));
                branches.add(branchModel);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return branches;
    }

    //Get Directorate Master From Database
    public ArrayList<DirectorateModel> findDirectorate(){
        ResultSet result;
        DirectorateModel directorateModel;
        ArrayList<DirectorateModel> directorateModels = new ArrayList<DirectorateModel>();
        try{
            Class.forName("oracle.jdbc.driver.OracleDriver");
            Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@192.168.177.70:1521:mncsvrpt", "IDV_REPORT", "idv_report");
//            Class.forName("com.mysql.cj.jdbc.Driver");
//            Connection conn = DriverManager.getConnection("JDBC:mysql://localhost:3306/mnc_database","root", "");
            String sql = "select * from SMI_DIRECTORATE";
            PreparedStatement ps = conn.prepareStatement(sql);
            result = ps.executeQuery();
            while (result.next()){
                directorateModel = new DirectorateModel(result.getString("DIVISION_ID"), result.getString("DIVISION_DESC"));
                directorateModels.add(directorateModel);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return directorateModels;
    }

    //Get SubCategory Master By Parent ID Category From Database
    public ArrayList<SubCategory> findSubCategory(String parent_id){
        System.out.println("ID : "+parent_id);
        ResultSet result;
        SubCategory subCategory;
        ArrayList<SubCategory> subCategories = new ArrayList<SubCategory>();
        try{
            Class.forName("oracle.jdbc.driver.OracleDriver");
            Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@192.168.177.70:1521:mncsvrpt", "IDV_REPORT", "idv_report");
//            Class.forName("com.mysql.cj.jdbc.Driver");
//            Connection conn = DriverManager.getConnection("JDBC:mysql://localhost:3306/mnc_database","root", "");
            String sql = "select * from smi_ticket_cat_lvl2 where parent_id = "+parent_id;
            PreparedStatement ps = conn.prepareStatement(sql);
            result = ps.executeQuery();
            while (result.next()){
                subCategory = new SubCategory(result.getString("id"), result.getString("category"), result.getString("parent_id"), result.getString("active"));
                subCategories.add(subCategory);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return subCategories;
    }


//    public void SaveImageIntoFolder(String imageName, String ext) throws IOException {
//        BufferedImage image = null;
//        System.out.println(imageName);
//        BASE64Decoder decoder = new BASE64Decoder();
//        byte[] decodeBytes = decoder.decodeBuffer(imageName);
//        try {
//            URL url = new URL("G://Magang//New folder//GambarDariAndroid//");
//            image = ImageIO.read(url);
//            ImageIO.write(image,ext,new File(String.valueOf(decodeBytes)));
//        } catch (MalformedURLException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
}
