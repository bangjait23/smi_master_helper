package Category;

public class CategoryModel {
    private String id_category;
    private String category;
    private String group_cat;

    public CategoryModel(String id_category, String category, String group_cat) {
        this.id_category = id_category;
        this.category = category;
        this.group_cat = group_cat;
    }

    public String getId_category() {
        return id_category;
    }

    public void setId_category(String id_category) {
        this.id_category = id_category;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getGroup_cat() {
        return group_cat;
    }

    public void setGroup_cat(String group_cat) {
        this.group_cat = group_cat;
    }

    @Override
    public String toString() {
        return "CategoryModel{" +
                "id_category='" + id_category + '\'' +
                ", category='" + category + '\'' +
                ", group_cat='" + group_cat + '\'' +
                '}';
    }
}
