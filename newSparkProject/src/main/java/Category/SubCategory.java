package Category;

public class SubCategory {
    private String id_subCategory;
    private String subCategory;
    private String parent_id;
    private String status;

    public SubCategory(String id_subCategory, String subCategory, String parent_id, String status) {
        this.id_subCategory = id_subCategory;
        this.subCategory = subCategory;
        this.parent_id = parent_id;
        this.status = status;
    }

    public String getId_subCategory() {
        return id_subCategory;
    }

    public void setId_subCategory(String id_subCategory) {
        this.id_subCategory = id_subCategory;
    }

    public String getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "SubCategory{" +
                "id_subCategory='" + id_subCategory + '\'' +
                ", subCategory='" + subCategory + '\'' +
                ", parent_id='" + parent_id + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
