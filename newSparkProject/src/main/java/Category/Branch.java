package Category;

public class Branch {
    private String prop_kode;
    private String org_id;
    private String org_name;

    public Branch(String prop_kode, String org_id, String org_name) {
        this.prop_kode = prop_kode;
        this.org_id = org_id;
        this.org_name = org_name;
    }

    public String getProp_kode() {
        return prop_kode;
    }

    public void setProp_kode(String prop_kode) {
        this.prop_kode = prop_kode;
    }

    public String getOrg_id() {
        return org_id;
    }

    public void setOrg_id(String org_id) {
        this.org_id = org_id;
    }

    public String getOrg_name() {
        return org_name;
    }

    public void setOrg_name(String org_name) {
        this.org_name = org_name;
    }

    @Override
    public String toString() {
        return "Branch{" +
                "prop_kode='" + prop_kode + '\'' +
                ", org_id='" + org_id + '\'' +
                ", org_name='" + org_name + '\'' +
                '}';
    }
}
