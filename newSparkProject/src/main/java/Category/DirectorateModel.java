package Category;

public class DirectorateModel {
    private String id_direktorat;
    private String directorate_name;

    public DirectorateModel(String id_direktorat, String directorate_name) {
        this.id_direktorat = id_direktorat;
        this.directorate_name = directorate_name;
    }

    public String getId_direktorat() {
        return id_direktorat;
    }

    public void setId_direktorat(String id_direktorat) {
        this.id_direktorat = id_direktorat;
    }

    public String getDirectorate_name() {
        return directorate_name;
    }

    public void setDirectorate_name(String directorate_name) {
        this.directorate_name = directorate_name;
    }

    @Override
    public String toString() {
        return "DirectorateModel{" +
                "id_direktorat='" + id_direktorat + '\'' +
                ", directorate_name='" + directorate_name + '\'' +
                '}';
    }
}
