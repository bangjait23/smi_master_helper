package DataUser;

import java.util.ArrayList;

public class DataUserModel {
    private String username;
    private String password;
    private String name;
    private String email;
    private String id;
    private String status_id;

    public DataUserModel() {
    }

    public DataUserModel(String id) {
        this.id = id;
    }

    public DataUserModel(String username, String password, String name, String email, String id, String status_id) {
        this.username = username;
        this.password = password;
        this.name = name;
        this.email = email;
        this.id = id;
        this.status_id = status_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus_id() {
        return status_id;
    }

    public void setStatus_id(String status_id) {
        this.status_id = status_id;
    }

    @Override
    public String toString() {
        return "DataUserModel{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", id=" + id +
                ", status_id=" + status_id +
                '}';
    }
}
