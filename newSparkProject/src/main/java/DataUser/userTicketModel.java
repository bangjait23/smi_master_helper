package DataUser;

public class userTicketModel {
   private String ticke_number;
   private String subject;
   private String phone_no;
   private String user_ext;
   private String message;
   private String cp_email1;
    private String cp_email2;
    private String cp_email3;
    private String cp_email4;
    private String cp_email5;
    private String date_created;
    private String dept_branch;
    private String category;
    private String status;

    public userTicketModel(String ticke_number, String subject, String phone_no, String user_ext, String message, String cp_email1, String cp_email2, String cp_email3, String cp_email4, String cp_email5, String date_created, String dept_branch, String category, String status) {
        this.ticke_number = ticke_number;
        this.subject = subject;
        this.phone_no = phone_no;
        this.user_ext = user_ext;
        this.message = message;
        this.cp_email1 = cp_email1;
        this.cp_email2 = cp_email2;
        this.cp_email3 = cp_email3;
        this.cp_email4 = cp_email4;
        this.cp_email5 = cp_email5;
        this.date_created = date_created;
        this.dept_branch = dept_branch;
        this.category = category;
        this.status = status;
    }

    public String getTicke_number() {
        return ticke_number;
    }

    public void setTicke_number(String ticke_number) {
        this.ticke_number = ticke_number;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getPhone_no() {
        return phone_no;
    }

    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }

    public String getUser_ext() {
        return user_ext;
    }

    public void setUser_ext(String user_ext) {
        this.user_ext = user_ext;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCp_email1() {
        return cp_email1;
    }

    public void setCp_email1(String cp_email1) {
        this.cp_email1 = cp_email1;
    }

    public String getCp_email2() {
        return cp_email2;
    }

    public void setCp_email2(String cp_email2) {
        this.cp_email2 = cp_email2;
    }

    public String getCp_email3() {
        return cp_email3;
    }

    public void setCp_email3(String cp_email3) {
        this.cp_email3 = cp_email3;
    }

    public String getCp_email4() {
        return cp_email4;
    }

    public void setCp_email4(String cp_email4) {
        this.cp_email4 = cp_email4;
    }

    public String getCp_email5() {
        return cp_email5;
    }

    public void setCp_email5(String cp_email5) {
        this.cp_email5 = cp_email5;
    }

    public String getDate_created() {
        return date_created;
    }

    public void setDate_created(String date_created) {
        this.date_created = date_created;
    }

    public String getDept_branch() {
        return dept_branch;
    }

    public void setDept_branch(String dept_branch) {
        this.dept_branch = dept_branch;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "userTicketModel{" +
                "ticke_number='" + ticke_number + '\'' +
                ", subject='" + subject + '\'' +
                ", phone_no='" + phone_no + '\'' +
                ", user_ext='" + user_ext + '\'' +
                ", message='" + message + '\'' +
                ", cp_email1='" + cp_email1 + '\'' +
                ", cp_email2='" + cp_email2 + '\'' +
                ", cp_email3='" + cp_email3 + '\'' +
                ", cp_email4='" + cp_email4 + '\'' +
                ", cp_email5='" + cp_email5 + '\'' +
                ", date_created='" + date_created + '\'' +
                ", dept_branch='" + dept_branch + '\'' +
                ", category='" + category + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
