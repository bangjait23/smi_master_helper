package uas2019.develops.stud11417025.smi_mnc.DataTicket;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import uas2019.develops.stud11417025.smi_mnc.Connection;
import uas2019.develops.stud11417025.smi_mnc.JSONParser;
import uas2019.develops.stud11417025.smi_mnc.R;
import uas2019.develops.stud11417025.smi_mnc.RegistrasiTicket.MainActivity;

public class DetailMyTicket extends AppCompatActivity {
    TextView ticket_number, subject, phone_no, user_ext, message, email, date_created, Category, status;
    Button close;
    JSONParser parser = new JSONParser();
    public static String id_ticket;
    public static final String TAG_STATUS = "status";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_my_ticket);

        ticket_number = (TextView) findViewById(R.id.id);
        subject = (TextView) findViewById(R.id.subject);
        phone_no = (TextView) findViewById(R.id.deptPhone);
        user_ext = (TextView) findViewById(R.id.phone);
        message = (TextView) findViewById(R.id.description);
        email = (TextView) findViewById(R.id.mailTo);
        date_created = (TextView) findViewById(R.id.dateTime);
        Category = (TextView) findViewById(R.id.category);
        status = (TextView) findViewById(R.id.status);
        close = (Button) findViewById(R.id.close);

        Intent mainIntent = getIntent();
        ticket_number.setText(mainIntent.getExtras().getString("id_ticket"));
        subject.setText(mainIntent.getExtras().getString("subject"));
        phone_no.setText(mainIntent.getExtras().getString("phone_no"));
        user_ext.setText(mainIntent.getExtras().getString("user_ext"));
        message.setText(mainIntent.getExtras().getString("message"));
        email.setText("1."+mainIntent.getExtras().getString("email1")+"\n2."+mainIntent.getExtras().getString("email2")+"\n3."
                +mainIntent.getExtras().getString("email3")+"\n4."+mainIntent.getExtras().getString("email4")+"\n5."
                +mainIntent.getExtras().getString("email5"));
        date_created.setText(mainIntent.getExtras().getString("date_created"));
        Category.setText(mainIntent.getExtras().getString("category"));
        status.setText(mainIntent.getExtras().getString("status"));
        id_ticket = mainIntent.getExtras().getString("id_ticket");


        //Button to close ticket
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(DetailMyTicket.this);

                builder.setTitle("Confirm");
                builder.setMessage("Are you sure to close this ticket  ?");

                builder.setPositiveButton("NO", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        // Do nothing but close the dialog

                        dialog.dismiss();
                    }
                });

                builder.setNegativeButton("YES", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        // Do nothing
                        new updateStatus().execute();
                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();
            }
        });

    }
    class updateStatus extends AsyncTask<String, String, String> {
        String success;
        @Override
        protected String doInBackground(String... args) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair(TAG_STATUS,"4"));
            try{
                JSONObject obj = parser.makeHttpRequest(Connection.url+"/update/ticket/"+id_ticket,"PUT", params);
                Log.d("Data na : ", obj.toString());
                success = obj.getString("responseMessage");
                if(!success.equals("user not found")){

                    finalize();
                }
                else {
                    return "gagal_database";
                }
            } catch (JSONException e) {
                Log.e("Error na : ",e.getMessage());
                e.printStackTrace();
                return "gagal_koneksi_or_exception";
            } catch (Throwable throwable) {
                Log.e("Error na : ",throwable.getMessage());
                throwable.printStackTrace();
            }

            return success;
        }
        protected void onPostExecute(String result) {
            if(!result.equals("user not found")){
                Toast.makeText(getApplicationContext(), "Your Ticket is Closed", Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(getApplicationContext(), "Cannot Close Your Ticket, Please Check Your Network", Toast.LENGTH_SHORT).show();
            }

        }
    }
}
