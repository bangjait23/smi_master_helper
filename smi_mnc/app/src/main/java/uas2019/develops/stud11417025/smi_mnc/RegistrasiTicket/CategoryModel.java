package uas2019.develops.stud11417025.smi_mnc.RegistrasiTicket;

/**
 * Created by test on 8/6/2019.
 */

public class CategoryModel {
    private String id_category;
    private String category;

    public String getId_category() {
        return id_category;
    }

    public void setId_category(String id_category) {
        this.id_category = id_category;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
