package uas2019.develops.stud11417025.smi_mnc.MyTicket;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uas2019.develops.stud11417025.smi_mnc.APIClient;
import uas2019.develops.stud11417025.smi_mnc.DataTicket.TicketModel;
import uas2019.develops.stud11417025.smi_mnc.R;
import uas2019.develops.stud11417025.smi_mnc.SessionManager;
import uas2019.develops.stud11417025.smi_mnc.Utils.ConnectionConnector;
import uas2019.develops.stud11417025.smi_mnc.smi_interface;

public class MyTicket extends Fragment implements SwipeRefreshLayout.OnRefreshListener{
    public static String mail;
    SessionManager sessionManager;
//    Context context = getActivity();
    public static String id_user;
    private ScrollView scrollTicket;
    private TextView errorticket;
    private RecyclerView recyclerviewTicket;
    private SwipeRefreshLayout  swiperefreshTicket;
    static SearchView search;

    ProgressDialog progressBar;
    Runnable runCekSelesai, runUtama;
    int progressBarStatus;
    Handler progressBarHandler = new Handler();
    private ConnectionConnector connDetect;
    private boolean isInternetPresent = false;
    private smi_interface smi;
    Button cari;
    private MyTicketRecycler recycler;
    private List<TicketModel> ticketModels;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_my_ticket, container, false);
        search = (SearchView) view.findViewById(R.id.search);
        sessionManager = new SessionManager(getContext());
        HashMap<String, String> user = sessionManager.getUserDetails();
        mail = user.get(SessionManager.email);
        id_user  = user.get(SessionManager.id);
        smi = APIClient.getRetrofit().create(smi_interface.class);

        scrollTicket           = (ScrollView) view.findViewById(R.id.scrollTicket);
        errorticket          = (TextView) view.findViewById(R.id.errorticket);
        recyclerviewTicket                 = (RecyclerView) view.findViewById(R.id.recyclerviewTicket);
        swiperefreshTicket             = (SwipeRefreshLayout) view.findViewById(R.id.swiperefreshTicket);

        recyclerviewTicket.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));

        swiperefreshTicket .setOnRefreshListener(this);
        swiperefreshTicket .setColorSchemeColors(Color.RED, Color.GREEN, Color.BLUE, Color.YELLOW);
        swiperefreshTicket .setEnabled(true);


//        ticketModels = new ArrayList<>();
//        recycler = new MyTicketRecycler(getActivity(), ticketModels, this);
//
//        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
//        recyclerviewTicket.setLayoutManager(mLayoutManager);
//        recyclerviewTicket.setItemAnimator(new DefaultItemAnimator());
//        recyclerviewTicket.setAdapter(recycler);

//        JsonArrayRequest arrayRequest = new JsonArrayRequest("http://192.168.179.78:9000/user", new com.android.volley.Response.Listener<JSONArray>() {
//            @Override
//            public void onResponse(JSONArray response) {
//                if (response == null) {
//                    Toast.makeText(getContext(), "Couldn't fetch the contacts! Pleas try again.", Toast.LENGTH_LONG).show();
//                    return;
//                }
//
//                List<TicketModel> items = new Gson().fromJson(response.toString(), new TypeToken<List<TicketModel>>() {
//                }.getType());
//
//                // adding contacts to contacts list
//                ticketModels.clear();
//                ticketModels.addAll(items);
//
//                // refreshing recycler view
//                recycler.notifyDataSetChanged();
//            }
//        }, new com.android.volley.Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.e(TAG, "Error: " + error.getMessage());
//                Toast.makeText(getContext(), "Error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
//            }
//        });
//        RequestQueue requestPriorityQueue = Volley.newRequestQueue(getActivity());
//        requestPriorityQueue.add(arrayRequest);


//        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//            @Override
//            public boolean onQueryTextSubmit(String query) {
//                recycler.getFilter().filter(query);
//                return false;
//            }
//
//            @Override
//            public boolean onQueryTextChange(String newText) {
//                recycler.getFilter().filter(newText);
//                return false;
//            }
//        });

        refreshItems();

        return view;
    }

    private void refreshItems() {
        scrollTicket.setVisibility(View.VISIBLE);
        errorticket.setText("Tidak ada suggestion");
        recyclerviewTicket .setVisibility(View.GONE);

        connDetect          = new ConnectionConnector(getContext());
        isInternetPresent   = connDetect.isConnectingToInternet();

        if (isInternetPresent) {
            loadticket();
        } else {
            if (swiperefreshTicket.isShown()) {
                swiperefreshTicket.setRefreshing(false);
            }
            Toast.makeText(getContext(), "Connection Error", Toast.LENGTH_SHORT).show();
        }
    }

    public void loadticket() {

        final Call<List<TicketModel>> call = smi.viewMyTicket(id_user);
        call.enqueue(new Callback<List<TicketModel>>() {
            @Override
            public void onResponse(Call<List<TicketModel>> call, Response<List<TicketModel>> response) {
                if(response.isSuccessful()){
                    getAllTicket(response.body());
                }else {
                    Log.e("Error na : ", String.valueOf(response.isSuccessful()));

                    if (swiperefreshTicket.isShown()) {
                        swiperefreshTicket.setRefreshing(false);
                    }

                    scrollTicket.setVisibility(View.VISIBLE);
                    errorticket.setText("Tidak ada data suggestion 1");

                    recyclerviewTicket.setVisibility(View.GONE);

                    Log.e("Error Message", response.message());
                }
            }

            @Override
            public void onFailure(Call<List<TicketModel>> call, Throwable t) {
                Log.e("Error na : ", t.getMessage());
                if (swiperefreshTicket.isShown()) {
                    swiperefreshTicket.setRefreshing(false);
                }

                Log.e("Message", t.getMessage());
                scrollTicket.setVisibility(View.VISIBLE);
                errorticket.setText("Tidak ada data suggestion 2");

                recyclerviewTicket.setVisibility(View.GONE);
            }
        });



    }
    public  void getAllTicket(List<TicketModel> body){
        if (swiperefreshTicket.isShown()) {
            swiperefreshTicket.setRefreshing(false);
        }

//        List<TicketModel> listdump =new ArrayList<>();
//
//        for (int i = 0; i < body.size();i++ ){
//            String ticket = body.get(i).getTicke_number();
//            String subject = body.get(i).getSubject();
//            String phone_no = body.get(i).getPhone_no();
//            String user_ext = body.get(i).getUser_ext();
//            String message = body.get(i).getMessage();
//            String cp_email1 = body.get(i).getCp_email1();
//            String cp_email2 = body.get(i).getCp_email2();
//            String cp_email3 = body.get(i).getCp_email3();
//            String cp_email4 = body.get(i).getCp_email4();
//            String cp_email5 = body.get(i).getCp_email5();
//            String date_ticket = body.get(i).getDate_created();
//            String branch = body.get(i).getDept_branch();
//            String category = body.get(i).getCategory();
//            String subdate = date_ticket.substring(0,9);
//            String status = body.get(i).getStatus();
//            TicketModel item = new TicketModel(ticket,subject,phone_no, user_ext, message, cp_email1,cp_email2,cp_email3,cp_email4,cp_email5,subdate,branch, category, status);
//            listdump.add(item);
//
//        }
//
//        MyTicketAdapter data = new MyTicketAdapter(getContext(), R.layout.my_ticket_item,listdump);
//        grid = (GridView) getActivity().findViewById(R.id.gridView1);
//        grid.setAdapter(data);

        if (body != null) {
            if (body.size() > 0) {
                scrollTicket .setVisibility(View.GONE);
                recyclerviewTicket.setVisibility(View.VISIBLE);
                LinearLayoutManager layoutParams = new LinearLayoutManager(getActivity());
                layoutParams.setOrientation(LinearLayoutManager.VERTICAL);
                recyclerviewTicket.setLayoutManager(layoutParams);
                MyTicketRecycler adapter = new MyTicketRecycler(getContext(),body);
                recyclerviewTicket.setAdapter(adapter);
            } else {
                scrollTicket.setVisibility(View.VISIBLE);
                errorticket.setText("Tidak ada data suggestion");

                recyclerviewTicket.setVisibility(View.GONE);
            }
        } else {
            scrollTicket.setVisibility(View.VISIBLE);
            errorticket.setText("Tidak ada data suggestion");
            recyclerviewTicket.setVisibility(View.GONE);
        }

    }

    @Override
    public void onRefresh() {
        refreshItems();
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if ( swiperefreshTicket!=null && swiperefreshTicket.isShown() ){
            swiperefreshTicket.setRefreshing(false);
        }
    }

//    @Override
//    public void TikcetModelSelected(TicketModel ticketModel) {
//
//    }
}
