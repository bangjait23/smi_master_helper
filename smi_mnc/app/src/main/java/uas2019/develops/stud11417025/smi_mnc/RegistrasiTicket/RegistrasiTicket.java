package uas2019.develops.stud11417025.smi_mnc.RegistrasiTicket;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import uas2019.develops.stud11417025.smi_mnc.JSONParser;
import uas2019.develops.stud11417025.smi_mnc.R;
import uas2019.develops.stud11417025.smi_mnc.SMI_Home;
import uas2019.develops.stud11417025.smi_mnc.SessionManager;


public class RegistrasiTicket extends Fragment {
    Fragment fragment = null;
    private ProgressDialog pg;
    Context context;
    SessionManager sessionManager;
    JSONParser parser = new JSONParser();
    public static String id_user, state;

    public static final String TAGID_TYPE = "id_type";
    public static final String TAGID_PRIORITY = "id_priority";
    public static final String TAGID_SUBCATEGORY = "id_SubCategory";
    public static final String TAGID_CATEGORY = "id_category";
    public static final String TAGNAME_BRANCH = "name_branch";
    public static final String TAGID_DIRECTORATE = "id_directorate";
    public static final String TAG_EMAIL1 = "email_1";
    public static final String TAG_EMAIL2 = "email_2";
    public static final String TAG_EMAIL3 = "email_3";
    public static final String TAG_EMAIL4 = "email_4";
    public static final String TAG_EMAIL5 = "email_5";
    public static final String TAG_SUBJECT = "subject";
    public static final String TAG_PHONEDEPT = "phoneDept";
    public static final String TAG_PHONE = "phone";
    public static final String TAG_DESC = "description";
    public static final String TAG_IDUSER = "id_user";
    public static final String TAG_DATETIME = "tanggal";
    public static final String TAG_GAMBAR = "has_attachment";
    public static String getImageName, nameGambar;


    //Penampung email
    public static String email1, email2, email3, email4, email5;

    //Library URL get Data Master
    private static final String url = "http://192.168.179.11:9000/Category/All";
    private static final String urlGetType = "http://192.168.179.11:9000/Type";
    private static final String urlGetPriority = "http://192.168.179.11:9000/Priority";
    private static final String urlGetBranch = "http://192.168.179.11:9000/Branch";
    private static final String urlGetDirectorate = "http://192.168.179.11:9000/Directorate";
    public static String URLGetSubCategory;

    SMI_Home smi = new SMI_Home();

    //Library List Data Master
    List<String> ValueIdCategory = new ArrayList<String>();
    List<String> ValueNameCategory = new ArrayList<String>();
    List<String> ValueIdType = new ArrayList<String>();
    List<String> ValueIdPriority = new ArrayList<String>();
    List<String> ValueNameBranch = new ArrayList<String>();
    List<String> ValueIdDirectorate = new ArrayList<String>();
    List<String> ValueIdSubcategory = new ArrayList<String>();

    public static String IDType, IDPriority, BranchName, IDDirectorate, IDSubcategory;


    public static String parent_id;
    private static final int CAMERA_REQUEST_CODE = 777;
    public List<EditText> test = new ArrayList<EditText>();;
    public String getEmail;
    private Bitmap bitmap;
    //    String url = "";
    private static final int RESULT_LOAD_IMG = 1;
    String imgDecodableString;

    EditText description;
    EditText subject;
    EditText phonedept;
    EditText pon;
    Spinner cat, status, type, subcategory, branch, directorate;
    Button save, addEmail;
    ImageView camera, file, gambar;

    public static String subj, deptPhone, user_ext, message;


    //Array to Fetch Data from database
    ArrayList<String> catArray = new ArrayList<String>();
    ArrayList<String> TypeArray = new ArrayList<String>();
    ArrayList<String> PriorityArray = new ArrayList<String>();
    ArrayList<String> BranchArray = new ArrayList<String>();
    ArrayList<String> DirectorateArray = new ArrayList<String>();
    ArrayList<String> SubCategoryArray = new ArrayList<String>();

    JSONObject result = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        test.clear();
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_registrasi_ticket, container, false);
        //----------------------------Get Type Drop Down----------------------------//
        type = (Spinner) view.findViewById(R.id.type);
        final ArrayAdapter<String> TypeAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, TypeArray);
        type.setAdapter(TypeAdapter);
        JsonArrayRequest typerequest = new JsonArrayRequest(Request.Method.GET, urlGetType, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                try {
                    for (int i = 0; i < response.length(); i++){
                        result = response.getJSONObject(i);
                        ValueIdType.add(result.getString("id_type"));
                        TypeArray.add(result.getString("name_type"));
                        TypeAdapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error : ",error.getMessage());
            }
        });
        RequestQueue requestTypeQueue = Volley.newRequestQueue(getActivity());
        requestTypeQueue.add(typerequest);
        type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                IDType = ValueIdType.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        subcategory = (Spinner) view.findViewById(R.id.subcategory);
        //----------------------------Get Category Drop Down----------------------------//
        cat = (Spinner) view.findViewById(R.id.category);
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, catArray);
        cat.setAdapter(adapter);
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                try {
                    for (int i = 0; i < response.length(); i++){
                        result = response.getJSONObject(i);
                        ValueIdCategory.add(result.getString("id_category"));
                        ValueNameCategory.add(result.getString("category"));
                        catArray.add(result.getString("category"));
                        adapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error : ",error.getMessage());
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(request);

        //Get ID Data Master Category
        cat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String id_get = ValueIdCategory.get(position);
                parent_id = id_get;
                String neCat = ValueNameCategory.get(position);
                URLGetSubCategory = "http://192.168.179.11:9000/Category/"+id_get+"/SubCategory";
                Toast.makeText(getContext(), "Anda Memilih ID Category : "+id_get+" dan Category : "+neCat, Toast.LENGTH_SHORT).show();
                SubCategoryArray.clear();

                //----------------------------Get Sub Category Drop Down----------------------------//
                final ArrayAdapter<String> SubCategoryAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, SubCategoryArray);
                subcategory.setAdapter(SubCategoryAdapter);
                JsonArrayRequest SubCategoryRequest = new JsonArrayRequest(Request.Method.GET, URLGetSubCategory, null, new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            for (int i = 0; i < response.length(); i++){
                                result = response.getJSONObject(i);
                                ValueIdSubcategory.add(result.getString("id_subCategory"));
                                SubCategoryArray.add(result.getString("subCategory"));
                                SubCategoryAdapter.notifyDataSetChanged();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Error : ",error.getMessage());
                    }
                });
                RequestQueue requestSubCategoryQueue = Volley.newRequestQueue(getActivity());
                requestSubCategoryQueue.add(SubCategoryRequest);

                //Get ID Data Master Sub Category
                subcategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        IDSubcategory = ValueIdSubcategory.get(position);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



        //----------------------------Get Priority Drop Down----------------------------//
        status = (Spinner) view.findViewById(R.id.priority);
        final ArrayAdapter<String> PriorityAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, PriorityArray);
        status.setAdapter(PriorityAdapter);
        JsonArrayRequest PriorityRequest = new JsonArrayRequest(Request.Method.GET, urlGetPriority, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                try {
                    for (int i = 0; i < response.length(); i++){
                        result = response.getJSONObject(i);
                        ValueIdPriority.add(result.getString("id"));
                        PriorityArray.add(result.getString("status"));
                        PriorityAdapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error : ",error.getMessage());
            }
        });
        RequestQueue requestPriorityQueue = Volley.newRequestQueue(getActivity());
        requestPriorityQueue.add(PriorityRequest);
        //Get ID Data Master Priority
        status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                IDPriority = ValueIdPriority.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



        //----------------------------Get Priority Drop Down----------------------------//
        branch = (Spinner) view.findViewById(R.id.branch);
        final ArrayAdapter<String> BranchAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, BranchArray);
        branch.setAdapter(BranchAdapter);
        JsonArrayRequest BranchRequest = new JsonArrayRequest(Request.Method.GET, urlGetBranch, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                try {
                    for (int i = 0; i < response.length(); i++){
                        result = response.getJSONObject(i);
                        ValueNameBranch.add(result.getString("org_name"));
                        BranchArray.add(result.getString("org_name"));
                        BranchAdapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error : ",error.getMessage());
            }
        });
        RequestQueue requestBranchQueue = Volley.newRequestQueue(getActivity());
        requestBranchQueue.add(BranchRequest);
        //Get ID Data Maste Branch
        branch.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                BranchName = ValueNameBranch.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



        //----------------------------Get Directorate Drop Down----------------------------//
        directorate = (Spinner) view.findViewById(R.id.directorate);
        final ArrayAdapter<String> DirectorateAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, DirectorateArray);
        directorate.setAdapter(DirectorateAdapter);
        JsonArrayRequest DirectorateRequest = new JsonArrayRequest(Request.Method.GET, urlGetDirectorate, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                try {
                    for (int i = 0; i < response.length(); i++){
                        result = response.getJSONObject(i);
                        ValueIdDirectorate.add(result.getString("id_direktorat"));
                        DirectorateArray.add(result.getString("directorate_name"));
                        DirectorateAdapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error : ",error.getMessage());
            }
        });
        RequestQueue requestDirectorateQueue = Volley.newRequestQueue(getActivity());
        requestDirectorateQueue.add(DirectorateRequest);
        //Get ID Data Master Directorate
        directorate.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                IDDirectorate = ValueIdDirectorate.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });




        camera = (ImageView) view.findViewById(R.id.camera);
        gambar = (ImageView) view.findViewById(R.id.gambar);
        file = (ImageView) view.findViewById(R.id.file);
        save = (Button) view.findViewById(R.id.save);
        addEmail = (Button) view.findViewById(R.id.addEmail);
        sessionManager = new SessionManager(getContext());
        HashMap<String, String> user = sessionManager.getUserDetails();
        id_user = user.get(SessionManager.id);

        //Get Image from camera
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(takePictureIntent, CAMERA_REQUEST_CODE);
                }
        });

        //Get Image From gallery
        file.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentgallery = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        // eksekusi intent
                startActivityForResult(intentgallery, RESULT_LOAD_IMG);
            }
        });
        final int[] count = {0};
            addEmail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(count[0] <= 5){
                        EditText email;
                        final LinearLayout linear = (LinearLayout) view.findViewById(R.id.emailLayout);
                        View inflate = getActivity().getLayoutInflater().inflate(R.layout.edit_text,null);
                        email = (EditText) inflate.findViewById(R.id.email);
                        email.setHint("Email ke : "+count[0]);
                        test.add(email);
                        linear.addView(inflate);
                    }
                    else {
                        Toast.makeText(getActivity(), "Udah Pas", Toast.LENGTH_SHORT).show();
                    }
                    count[0]++;
                }
            });

        subject = (EditText) view.findViewById(R.id.subject);
        subj = subject.getText().toString();

        phonedept = (EditText) view.findViewById(R.id.pnb);
        deptPhone = phonedept.getText().toString();

        pon = (EditText) view.findViewById(R.id.pen);
        user_ext = pon.getText().toString();

        description = (EditText) view.findViewById(R.id.deskripsi);
        message = description.getText().toString();
        //Send Data All
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(subject.getText().toString().equals("") || phonedept.getText().toString().equals("") ||
                        pon.getText().toString().equals("") || description.getText().toString().equals("")){
                    Toast.makeText(getContext(), "Field Cannot Empty", Toast.LENGTH_SHORT).show();
                }
                else if(pon.getText().toString().length()>8){
                    Toast.makeText(context, "User Ext Maximum is 8", Toast.LENGTH_SHORT).show();
                }
                else{
                    new createTickets().execute();
                    subject.setText("");
                    phonedept.setText("");
                    pon.setText("");
                    description.setText("");
                    pg.dismiss();
                    startActivity(new Intent(getContext(), MainActivity.class));
                }
            }
        });
        return view;



    }
    //Get Image Process From Camera
    public void onActivityResult(int requestCode,int resultCode,Intent data){
        super.onActivityResult(requestCode,resultCode,data);
        switch (requestCode){
            case(CAMERA_REQUEST_CODE):
                if(resultCode == Activity.RESULT_OK)
                {
                    bitmap = (Bitmap) data.getExtras().get("data");
                    gambar.setImageBitmap(bitmap);
                }
                break;
            case(RESULT_LOAD_IMG):
                if(resultCode == Activity.RESULT_OK)
                {
                    Uri returnUri = data.getData();
                    Bitmap bitmapImage = null;
                    try {
                        bitmapImage = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), returnUri);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    gambar.setImageBitmap(bitmapImage);
//                    getImageName = String.valueOf(gambar);
                }
                break;
        }
    }
    public String getStringImage(Bitmap bmp){
        String encodedImage;
        if(bmp==null){
            return "0";
        }
        else{
            ByteArrayOutputStream baos =new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.JPEG,100,baos);
            byte[] imageBytes = baos.toByteArray();
            encodedImage = Base64.encodeToString(imageBytes,Base64.DEFAULT);
            return encodedImage;
        }

    }


    public String getDateTime(){
        DateFormat date = new SimpleDateFormat("dd-M-yy");
        Date dates = new Date();
        String waktu = date.format(dates);
        return waktu;
    }

    //Class for Upload Data
    class createTickets extends AsyncTask<String, String, String>{
        public String success;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pg = new ProgressDialog(getContext());
            pg.setMessage("Please waiting...");
            pg.setIndeterminate(false);
            pg.setCancelable(false);
            pg.show();
        }

        @Override
        protected String doInBackground(String... args) {
            List<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair(TAG_IDUSER, id_user));
            param.add(new BasicNameValuePair(TAGID_CATEGORY, parent_id));
            param.add(new BasicNameValuePair(TAG_SUBJECT, subject.getText().toString()));
            param.add(new BasicNameValuePair(TAG_DESC, description.getText().toString()));
            param.add(new BasicNameValuePair(TAGNAME_BRANCH, BranchName));
            param.add(new BasicNameValuePair(TAG_PHONE, pon.getText().toString()));
            for (int i = 0; i<test.size(); i++){
                if(i==0){
                    param.add(new BasicNameValuePair(TAG_EMAIL1,  test.get(i).getText().toString()));
                }
                if(i==1){
                    param.add(new BasicNameValuePair(TAG_EMAIL2,  test.get(i).getText().toString()));
                }
                if(i==2){
                    param.add(new BasicNameValuePair(TAG_EMAIL3, test.get(i).getText().toString()));
                }
                if(i==3){
                    param.add(new BasicNameValuePair(TAG_EMAIL4, test.get(i).getText().toString()));
                }
                if(i==4) {
                    param.add(new BasicNameValuePair(TAG_EMAIL5, test.get(i).getText().toString()));
                }
            }
            param.add(new BasicNameValuePair(TAG_DATETIME, "28-MAR-16"));
            getImageName = getStringImage(bitmap);
            param.add(new BasicNameValuePair(TAG_PHONEDEPT, phonedept.getText().toString()));
            param.add(new BasicNameValuePair(TAGID_PRIORITY, IDPriority));
            param.add(new BasicNameValuePair(TAGID_SUBCATEGORY, IDSubcategory));
            param.add(new BasicNameValuePair(TAGID_DIRECTORATE, IDDirectorate));
            if(getImageName==null){
                param.add(new BasicNameValuePair(TAG_GAMBAR, "0"));
            }
            else{
                param.add(new BasicNameValuePair(TAG_GAMBAR, getImageName));
            }
            System.out.println("Data : "+param.toString());
            try{
                JSONObject obj = parser.makeHttpRequest("http://192.168.179.11:9000/user/add","POST", param);
                Log.e("Data na : ", obj.toString());
                success = obj.getString("responseMessage");
                if(success.equals("success")){
                    finalize();
                }
                else {
                    return "gagal_database";
                }
            } catch (JSONException e) {
                Log.e("Error na : ",e.getMessage());
                e.printStackTrace();
                return "gagal_koneksi_or_exception";
            } catch (Throwable throwable) {
                Log.e("Error na : ",throwable.getMessage());
                throwable.printStackTrace();
            }

            return success;
        }

        protected void onPostExecute(String result) {
            if(result.equals("success")){
                Toast.makeText(getActivity(), "Register Ticket Success", Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(getActivity(), "Network Error", Toast.LENGTH_SHORT).show();
            }

        }
    }
}