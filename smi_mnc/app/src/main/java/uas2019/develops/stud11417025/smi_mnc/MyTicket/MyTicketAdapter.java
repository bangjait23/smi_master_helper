package uas2019.develops.stud11417025.smi_mnc.MyTicket;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import uas2019.develops.stud11417025.smi_mnc.DataTicket.TicketModel;
import uas2019.develops.stud11417025.smi_mnc.DetailComment.CommentMyTicket;
import uas2019.develops.stud11417025.smi_mnc.JSONParser;
import uas2019.develops.stud11417025.smi_mnc.R;

/**
 * Created by test on 7/19/2019.
 */

public class MyTicketAdapter extends ArrayAdapter<TicketModel> {
    public static final String TAG_STATUS = "status";
    public static String id_ticket;
    Context context = null;
    JSONParser parser = new JSONParser();


    public MyTicketAdapter(Context context, int resource, List<TicketModel> objects) {
        super(context, resource, objects);
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.ticket_item, parent, false

            );
            final TicketModel ticketModel = getItem(position);
            TextView ticket_number = (TextView) convertView.findViewById(R.id.id);
            ticket_number.setText(ticketModel.getTicke_number());

            TextView subject = (TextView) convertView.findViewById(R.id.subject);
            subject.setText(ticketModel.getSubject());

            TextView phone_no = (TextView) convertView.findViewById(R.id.deptPhone);
            phone_no.setText(ticketModel.getPhone_no());

            TextView user_ext = (TextView) convertView.findViewById(R.id.phone);
            user_ext.setText(ticketModel.getUser_ext());

            TextView message = (TextView) convertView.findViewById(R.id.description);
            message.setText(ticketModel.getMessage());

            TextView email = (TextView) convertView.findViewById(R.id.email);
            email.setText(ticketModel.getCp_email1()+ticketModel.getCp_email2()+ticketModel.getCp_email3()+ticketModel.getCp_email4()+ticketModel.getCp_email5());

            TextView date_created = (TextView) convertView.findViewById(R.id.dateTime);
            date_created.setText(ticketModel.getDate_created());



            TextView Category = (TextView) convertView.findViewById(R.id.category);
            Category.setText(ticketModel.getCategory());

            TextView status = (TextView) convertView.findViewById(R.id.status);
            status.setText(ticketModel.getStatus());


            Button comment = (Button) convertView.findViewById(R.id.comment);
            comment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    id_ticket = ticketModel.getTicke_number();
                    getContext().startActivity(new Intent(getContext(), CommentMyTicket.class));
                }
            });

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Dialog dialog = new Dialog(getContext());
                    dialog.setContentView(R.layout.detail_my_tikcet);
                    dialog.setTitle("Detail");

                    TextView ticket_number = (TextView) dialog.findViewById(R.id.id);
                    ticket_number.setText(ticketModel.getTicke_number());

                    TextView subject = (TextView) dialog.findViewById(R.id.subject);
                    subject.setText(ticketModel.getSubject());

                    TextView phone_no = (TextView) dialog.findViewById(R.id.phoneDept);
                    phone_no.setText(ticketModel.getPhone_no());

                    TextView user_ext = (TextView) dialog.findViewById(R.id.phone);
                    user_ext.setText(ticketModel.getUser_ext());

                    TextView message = (TextView) dialog.findViewById(R.id.description);
                    message.setText(ticketModel.getMessage());

                    TextView email = (TextView) dialog.findViewById(R.id.email);
                    email.setText("1."+ticketModel.getCp_email1()+"\n2."+ticketModel.getCp_email2()+"\n3."+ticketModel.getCp_email3()+
                            "\n4."+ticketModel.getCp_email4()+"\n5."+ticketModel.getCp_email5());

                    TextView date_created = (TextView) dialog.findViewById(R.id.tanggal);
                    date_created.setText(ticketModel.getDate_created());

                    TextView Category = (TextView) dialog.findViewById(R.id.category);
                    Category.setText(ticketModel.getCategory());

                    TextView status = (TextView) dialog.findViewById(R.id.status);
                    status.setText(ticketModel.getStatus());
                    Button close = (Button) dialog.findViewById(R.id.close);
                    close.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

                            builder.setTitle("Confirm");
                            builder.setMessage("Are you sure to close this ticket  ?");

                            builder.setPositiveButton("NO", new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int which) {
                                    // Do nothing but close the dialog

                                    dialog.dismiss();
                                }
                            });

                            builder.setNegativeButton("YES", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    // Do nothing
                                    new updateStatus().execute();
                                }
                            });

                            AlertDialog alert = builder.create();
                            alert.show();
                        }
                    });
                    if(ticketModel.getStatus().equals("CLOSE")){
                        close.setEnabled(false);
//                        comment.setEnabled(false);
                    }
                    // set width for dialog
                    int width = (int) (getContext().getResources().getDisplayMetrics().widthPixels * 1);
                    // set height for dialog
                    int height = (int) (getContext().getResources().getDisplayMetrics().heightPixels * 0.96);
                    dialog.getWindow().setLayout(width, height);
                    dialog.show();
                }
            });
        }
        return convertView;
    }
    class updateStatus extends AsyncTask<String, String, String>{
        String status = "CLOSE";
        String success;
        @Override
        protected String doInBackground(String... args) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            try{
                params.add(new BasicNameValuePair(TAG_STATUS, status));

                JSONObject obj = parser.makeHttpRequest("http://192.168.179.78:9000/update/ticket/"+id_ticket,"PUT", params);
                Log.d("Data na : ", obj.toString());
                success = obj.getString("responseMessage");
                if(!success.equals("user not found")){

                    finalize();
                }
                else {
                    return "gagal_database";
                }
            } catch (JSONException e) {
                Log.e("Error na : ",e.getMessage());
                e.printStackTrace();
                return "gagal_koneksi_or_exception";
            } catch (Throwable throwable) {
                Log.e("Error na : ",throwable.getMessage());
                throwable.printStackTrace();
            }

            return success;
        }
        protected void onPostExecute(String result) {
            if(!result.equals("user not found")){
                Toast.makeText(getContext(), "Your Ticket is Closed", Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(getContext(), "Cannot Close Your Ticket, Please Check Your Network", Toast.LENGTH_SHORT).show();
            }

        }
    }
    public static String getId_ticket(){
        return id_ticket;
    }
//    private String getCurrentDate() {
//        Date current = new Date();
//        SimpleDateFormat frmt = new SimpleDateFormat("dd-MM-yyyy");
//        String dateString = frmt.format(current);
//
//        return dateString;
//    }
}
