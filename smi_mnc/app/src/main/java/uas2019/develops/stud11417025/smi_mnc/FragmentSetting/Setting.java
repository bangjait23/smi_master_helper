package uas2019.develops.stud11417025.smi_mnc.FragmentSetting;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import uas2019.develops.stud11417025.smi_mnc.Connection;
import uas2019.develops.stud11417025.smi_mnc.JSONParser;
import uas2019.develops.stud11417025.smi_mnc.R;
import uas2019.develops.stud11417025.smi_mnc.RegistrasiTicket.MainActivity;
import uas2019.develops.stud11417025.smi_mnc.SessionManager;

public class Setting extends Fragment {

    Button logout, change;
    SessionManager sessionManager;
    public String password;
    JSONParser parser = new JSONParser();
    public String id_user;
    public static String TAG_NEWPASS = "password";
    TextView oldpass, newPass;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_setting, container, false);

        sessionManager = new SessionManager(getContext());
        HashMap<String, String> user = sessionManager.getUserDetails();
        password = user.get(SessionManager.password);



        id_user = user.get(SessionManager.id);
        logout = (Button) view.findViewById(R.id.Logout);
        change = (Button) view.findViewById(R.id.ChangePassword);
        oldpass = (TextView) view.findViewById(R.id.oldPassword);
        newPass = (TextView) view.findViewById(R.id.newPassword);

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sessionManager.logout();
                Toast.makeText(getContext(), "Thanks To Visit Our Application", Toast.LENGTH_SHORT).show();
            }
        });
        change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String passEnc = null;
                try {
                    passEnc = getPassword();
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }
                if(oldpass.getText().toString().equals("") || newPass.getText().toString().equals("")){
                    Toast.makeText(getContext(), "Field Cannot Empty", Toast.LENGTH_SHORT).show();
                }
                else{
                        if(passEnc.equals(password)){
                            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                            builder
                                    .setTitle("Erase hard drive")
                                    .setMessage("Are you sure?")
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                           new change().execute();
                                        }
                                    })
                                    .setNegativeButton("No", null)
                                    .show();
                            Toast.makeText(getActivity(), "Password Lama sama", Toast.LENGTH_SHORT).show();
                        }
                        else{
                            Toast.makeText(getContext(), "Your Old Password Doesn't match", Toast.LENGTH_SHORT).show();
                        }
                }
            }
        });

        return view;
    }

    // class Change Password Process
    class change extends AsyncTask<String, String , String>{
        String success;
        @Override
        protected String doInBackground(String... args) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            try{
                params.add(new BasicNameValuePair(TAG_NEWPASS, newPass.getText().toString()));

                JSONObject obj = parser.makeHttpRequest(Connection.url+"/user/password/"+id_user,"PUT", params);

                success = obj.getString("responseMessage");
                if(success.equals("Update Password Success")){
                    startActivity(new Intent(getContext(), MainActivity.class));
                    finalize();
                }
                else {
                    return "gagal_database";
                }
            }catch (JSONException e) {
                Log.e("Error na : ",e.getMessage());
                e.printStackTrace();
                return "gagal_koneksi_or_exception";
            } catch (Throwable throwable) {
                Log.e("Error na : ",throwable.getMessage());
                throwable.printStackTrace();
            }
            return success;
        }
        @Override
        protected void onPostExecute(String success) {
            if(success.equals("Update Password Success")){
                Toast.makeText(getContext(), success, Toast.LENGTH_SHORT).show();
                oldpass.setText("");
                newPass.setText("");
            }
            else{
                Toast.makeText(getContext(), success, Toast.LENGTH_SHORT).show();
            }
        }
    }
    public String getPassword() throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        byte[] hashInBytes = md.digest(oldpass.getText().toString().getBytes(StandardCharsets.UTF_8));

        StringBuilder sb = new StringBuilder();
        for(byte b : hashInBytes) {
            sb.append(String.format("%02x", b));
        }

        return sb.toString();
    }
}
