package uas2019.develops.stud11417025.smi_mnc.DetailComment;

import com.google.gson.annotations.SerializedName;

/**
 * Created by test on 7/3/2019.
 */

public class CommentModel {
    @SerializedName("id")
    private String id;
    @SerializedName("id_ticket")
    private String id_ticket;
    @SerializedName("tanggapan")
    private String tanggapan;
    @SerializedName("user")
    private String user;
    @SerializedName("name")
    private String name;

    public CommentModel(String id, String id_ticket, String tanggapan, String user, String name) {
        this.id = id;
        this.id_ticket = id_ticket;
        this.tanggapan = tanggapan;
        this.user = user;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId_ticket() {
        return id_ticket;
    }

    public void setId_ticket(String id_ticket) {
        this.id_ticket = id_ticket;
    }

    public String getTanggapan() {
        return tanggapan;
    }

    public void setTanggapan(String tanggapan) {
        this.tanggapan = tanggapan;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
