package uas2019.develops.stud11417025.smi_mnc;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import uas2019.develops.stud11417025.smi_mnc.DataTicket.TicketModel;
import uas2019.develops.stud11417025.smi_mnc.DetailComment.CommentModel;

/**
 * Created by test on 7/1/2019.
 */

public interface smi_interface {
    @GET("user")
    Call<List<TicketModel>> viewAllTicket();
    @GET("comment/{id_ticket}")
    Call<List<CommentModel>> viewComment(@Path("id_ticket") String id_ticket);
    @GET("ticket/{id_user}")
    Call<List<TicketModel>> viewMyTicket(@Path("id_user")String id_user);
    @GET("getData/user/{email}")
    Call<List<userModel>> getDataUser(@Path("email")String username);
    @GET("search/ticket/{id}")
    Call<List<TicketModel>> SearchTicketById(@Path("id")String id);
}
