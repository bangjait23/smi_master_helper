package uas2019.develops.stud11417025.smi_mnc.MyTicket;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import uas2019.develops.stud11417025.smi_mnc.DataTicket.DetailMyTicket;
import uas2019.develops.stud11417025.smi_mnc.DataTicket.TicketModel;
import uas2019.develops.stud11417025.smi_mnc.DetailComment.CommentMyTicket;
import uas2019.develops.stud11417025.smi_mnc.JSONParser;
import uas2019.develops.stud11417025.smi_mnc.R;

/**
 * Created by test on 8/16/2019.
 */

public class MyTicketRecycler extends RecyclerView.Adapter<MyTicketRecycler.ViewHolder> {
    private Context context;
    private List<TicketModel> itemData = new ArrayList<>();
    private View itemLayoutView;
    private static String id_ticket;
    JSONParser parser = new JSONParser();
    private List<TicketModel> ticketListFiltered;
//    private MyTicketRecyclerListener listener;

    public MyTicketRecycler(Context _context, List<TicketModel> list) {
        this.context    = _context;
        this.itemData   = list;
//        this.listener = ticketListFiltered;
    }
    public void notify(List<TicketModel> list) {
        if (itemData != null) {
            itemData.clear();
            itemData.addAll(list);

        } else {
            itemData = list;
        }
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        itemLayoutView          = LayoutInflater.from(parent.getContext()).inflate(R.layout.ticket_item, parent, false);
        ViewHolder viewHolder   = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        if (itemData.size() > 0) {
            final TicketModel item = itemData.get(position);
            holder.ticket_number.setText(item.getTicke_number());
            holder.subject.setText(item.getTicke_number());
            holder.phone_no.setText(item.getTicke_number());
            holder.user_ext.setText(item.getUser_ext());
            holder.message.setText(item.getMessage());
            holder.email.setText(item.getCp_email1());
            holder.date_created.setText(item.getDate_created());
            holder.Category.setText(item.getCategory());
            holder.status.setText(item.getStatus());
            if(item.getStatus().equals("RESOLVED")){
                holder.reply.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(context, "Closed Ticket", Toast.LENGTH_SHORT).show();
                    }
                });
            }
            else{
                holder.reply.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new  Intent(context, CommentMyTicket.class);
                        intent.putExtra("id_ticket", item.getTicke_number());
                        context.startActivity(intent);
                    }
                });
            }
            holder.detail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent detailIntent = new Intent(context, DetailMyTicket.class);
                    detailIntent.putExtra("id_ticket", item.getTicke_number());
                    detailIntent.putExtra("subject", item.getSubject());
                    detailIntent.putExtra("phone_no", item.getPhone_no());
                    detailIntent.putExtra("user_ext", item.getUser_ext());
                    detailIntent.putExtra("message", item.getMessage());
                    detailIntent.putExtra("email1", item.getCp_email1());
                    detailIntent.putExtra("email2", item.getCp_email2());
                    detailIntent.putExtra("email3", item.getCp_email3());
                    detailIntent.putExtra("email4", item.getCp_email4());
                    detailIntent.putExtra("email5", item.getCp_email5());
                    detailIntent.putExtra("date_created", item.getDate_created());
                    detailIntent.putExtra("category",item.getCategory());
                    detailIntent.putExtra("status", item.getStatus());

                    Toast.makeText(context, "Data : "+item.getSubject(), Toast.LENGTH_SHORT).show();
                    context.startActivity(detailIntent);

                }
            });
        }
    }

    @Override
    public int getItemCount() {
        if (itemData != null && itemData.size() > 0) {
            return itemData.size();
        } else {
            return 1;
        }
    }

//    @Override
//    public Filter getFilter() {
//        return new Filter() {
//            @Override
//            protected FilterResults performFiltering(CharSequence constraint) {
//                String charString  = constraint.toString();
//                if(charString.isEmpty()){
//                    ticketListFiltered = itemData;
//                }else{
//                    List<TicketModel> filteredList = new ArrayList<>();
//                    for(TicketModel row : itemData){
//                        if(row.getTicke_number().toLowerCase().contains(charString.toLowerCase())){
//                            filteredList.add(row);
//                        }
//                    }
//                    ticketListFiltered = filteredList;
//                }
//                FilterResults filterResults = new FilterResults();
//                filterResults.values = ticketListFiltered;
//                return filterResults;
//            }
//
//            @Override
//            protected void publishResults(CharSequence constraint, FilterResults results) {
//                ticketListFiltered = (ArrayList<TicketModel>) results.values;
//                notifyDataSetChanged();
//            }
//        };
//    }
//    public interface MyTicketRecyclerListener {
//        void TikcetModelSelected(TicketModel ticketModel);
//    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout contohLinearLayout;
        private TextView ticket_number, subject, phone_no, user_ext, message, email, date_created, Category, status;
        Button reply, detail;
        private Context context;
        public ViewHolder(View itemView) {
            super(itemView);
            context= itemView.getContext();
              ticket_number = (TextView) itemLayoutView.findViewById(R.id.id);

              subject = (TextView) itemLayoutView.findViewById(R.id.subject);


              phone_no = (TextView) itemLayoutView.findViewById(R.id.deptPhone);


              user_ext = (TextView) itemLayoutView.findViewById(R.id.phone);


              message = (TextView) itemLayoutView.findViewById(R.id.description);


              email = (TextView) itemLayoutView.findViewById(R.id.email);


              date_created = (TextView) itemLayoutView.findViewById(R.id.dateTime);

              Category = (TextView) itemLayoutView.findViewById(R.id.category);


              status = (TextView)itemLayoutView.findViewById(R.id.status);

                reply = (Button) itemLayoutView.findViewById(R.id.comment);
               detail = (Button) itemLayoutView.findViewById(R.id.btnDetail);

        }
    }
}
