package uas2019.develops.stud11417025.smi_mnc.DetailComment;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import uas2019.develops.stud11417025.smi_mnc.R;

/**
 * Created by test on 7/3/2019.
 */

public class DetailCommentAdapter extends ArrayAdapter<CommentModel> {
    public DetailCommentAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<CommentModel> objects) {
        super(context, resource, objects);
    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.activity_detail_comment, parent, false
            );
            final CommentModel commentModel = getItem(position);

            TextView user = (TextView) convertView.findViewById(R.id.nameUser);
            user.setText(commentModel.getName());

            TextView comment = (TextView) convertView.findViewById(R.id.com);
            comment.setText(commentModel.getTanggapan());

            System.out.println("Data na : "+commentModel.getUser());
        }
        return convertView;
    }
}
