package uas2019.develops.stud11417025.smi_mnc.RegistrasiTicket;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextClock;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import uas2019.develops.stud11417025.smi_mnc.FragmentSetting.Setting;
import uas2019.develops.stud11417025.smi_mnc.MyTicket.MyTicket;
import uas2019.develops.stud11417025.smi_mnc.R;
import uas2019.develops.stud11417025.smi_mnc.SessionManager;

public class MainActivity extends AppCompatActivity {
    TextView name, tanggal, greeting, bagian;
    TextClock jam;
    public static SessionManager session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        session = new SessionManager(this);
        HashMap<String, String> user = session.getUserDetails();
        session = new SessionManager(this);
        name = (TextView) findViewById(R.id.name);
        name.setText(user.get(SessionManager.name));
        bagian = (TextView) findViewById(R.id.id);
        greeting = (TextView) findViewById(R.id.greeting);
        jam = (TextClock) findViewById(R.id.jam1);
        String time = getCurrentTime();
        int jam = Integer.parseInt(time.substring(0,2));
        int menit = Integer.parseInt(time.substring(3,5));
        int timeTotal = (jam*60)+menit;
        int pagi = 60;
        int btsPagi = 659;
        int siang = 660;
        int btsSiang = 900;
        int sore = 931;
        int btsSore = 1139;
        int malam = 1140;
        int btsmalam =  1440;
        if(timeTotal >= pagi && timeTotal <= btsPagi){
            greeting.setText("Good Morning, ");
        }
        else if(timeTotal >= siang && timeTotal <= btsSiang){
            greeting.setText("Good Day, ");
        }
        else if(timeTotal >= sore && timeTotal <= btsSore){
            greeting.setText("Good Afternoon, ");
        }
        else if(timeTotal >= malam && timeTotal <= btsmalam){
            greeting.setText("Good Night, ");
        }
        tanggal = (TextView) findViewById(R.id.tanggal);
        tanggal.setText(getCurrentDate());
        BottomNavigationView bottomNavigationView = (BottomNavigationView)findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Fragment frag = null;
                switch (item.getItemId()){
//                    case R.id.mt:
//                        frag = new DataTicket();
//                        bagian.setText("All Ticket");
//                        break;
                    case R.id.rt:
                        frag = new RegistrasiTicket();
                        bagian.setText("Register Ticket");
                        break;
                    case R.id.qa:
                        frag = new MyTicket();
                        bagian.setText("My Ticket");
                        break;
                    case R.id.Setting:
                        bagian.setText("Setting");
                        frag = new Setting();
                        break;
                }
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame, frag);
                transaction.commit();
                return true;
            }
        });
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame, new MyTicket());
        transaction.commit();
    }
    private String getCurrentDate() {
        Date current = new Date();
        SimpleDateFormat frmt = new SimpleDateFormat("EEEE, dd-MM-yyyy");
        String dateString = frmt.format(current);
        return dateString;
    }
    private String getCurrentTime() {
        Date current = new Date();
        SimpleDateFormat frmt = new SimpleDateFormat("HH:mm");
        String dateString = frmt.format(current);
        return dateString;
    }
}
