package uas2019.develops.stud11417025.smi_mnc.DetailComment;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uas2019.develops.stud11417025.smi_mnc.APIClient;
import uas2019.develops.stud11417025.smi_mnc.Connection;
import uas2019.develops.stud11417025.smi_mnc.JSONParser;
import uas2019.develops.stud11417025.smi_mnc.R;
import uas2019.develops.stud11417025.smi_mnc.RegistrasiTicket.MainActivity;
import uas2019.develops.stud11417025.smi_mnc.SessionManager;
import uas2019.develops.stud11417025.smi_mnc.smi_interface;

public class CommentMyTicket extends AppCompatActivity {
    private static final String TAG_SUCCESS = "Success";
    private static final String TAG_ID = "id_ticket";
    private static final String TAG_COMMENT = "tanggapan";
    private static final String TAG_ID_USER = "id_user";
    public static String id_tickets, id_user;
    public static String commnets;
    public static String mail;
    public static final int notifikasi = 1;
    GridView grid;
    EditText com;
    Button send;
    SessionManager session;
    JSONParser parser = new JSONParser();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment_my_ticket);

        session = new SessionManager(this);
        HashMap<String, String> user = session.getUserDetails();
        mail = user.get(SessionManager.email);

        Intent mainIntent = getIntent();
        id_tickets = mainIntent.getExtras().getString("id_ticket");

//        id_tickets2 = MyTicketAdapter.getId_ticket();
         id_user = user.get(SessionManager.id);

        com = (EditText) findViewById(R.id.txtcomment);
        send = (Button) findViewById(R.id.btnsend);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(com.getText().toString().equals("")){
                    Toast.makeText(CommentMyTicket.this, "Fill Your Comment", Toast.LENGTH_SHORT).show();
                }
                else{
                    commnets = com.getText().toString();
                    new createComment().execute();
                    //Make Notifiaksi
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    PendingIntent pendingIntent = PendingIntent.getActivity(CommentMyTicket.this
                            , notifikasi, intent, PendingIntent.FLAG_UPDATE_CURRENT);

                    // membuat komponen notifikasi
                    NotificationCompat.Builder builder = new NotificationCompat.Builder(CommentMyTicket.this);
                    Notification notification;
                    notification = builder.setSmallIcon(R.drawable.ic_mnc)
                            .setAutoCancel(true)
                            .setContentIntent(pendingIntent)
                            .setContentTitle("Subscriber Manajement Interactive")
                            .setLargeIcon(BitmapFactory.decodeResource(CommentMyTicket.this.getResources()
                                    , R.drawable.ic_mnc))
                            .setContentText("You Have a Commment")
                            .build();

                    notification.flags |= Notification.FLAG_AUTO_CANCEL;

                    NotificationManager notificationManager = (NotificationManager) CommentMyTicket.this
                            .getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.notify(notifikasi, notification);
                    finish();


                    Intent intent1 = getIntent();
                    finish();
                    startActivity(intent1);

                }
            }
        });


        smi_interface smi = APIClient.getRetrofit().create(smi_interface.class);
        final Call<List<CommentModel>> call = smi.viewComment(id_tickets);
        call.enqueue(new Callback<List<CommentModel>>() {
            @Override
            public void onResponse(Call<List<CommentModel>> call, Response<List<CommentModel>> response) {
                if(response.isSuccessful()){
                    getComment(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<CommentModel>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Nothing Comment to Show", Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void getComment(List<CommentModel> body) {
        DetailCommentAdapter commentAdapter = new DetailCommentAdapter(this, R.layout.ticket_item, body);
        grid = (GridView) findViewById(R.id.gridView1);
        grid.setAdapter(commentAdapter);
        commentAdapter.notifyDataSetChanged();
    }
    public static java.lang.String getCommnets() {
        return commnets;
    }


    // menambah komentar terhadap sebuat ticket
    class createComment extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... args) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();

            params.add(new BasicNameValuePair(TAG_ID, id_tickets));
            params.add(new BasicNameValuePair(TAG_COMMENT, CommentMyTicket.getCommnets()));
            params.add(new BasicNameValuePair(TAG_ID_USER, id_user));

            System.out.println("Data : "+params.toString());

            JSONObject obj = parser.makeHttpRequest(Connection.url+"/comment/add","POST", params);


            try{
                int success = obj.getInt(TAG_SUCCESS);
                if(success == 1){
                    finalize();
                }
                else {
                    return "gagal_database";
                }
            } catch (JSONException e) {
                e.printStackTrace();
                return "gagal_koneksi_or_exception";
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }

            return "sukses";
        }
    }
}
