package uas2019.develops.stud11417025.smi_mnc;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import java.util.HashMap;

import uas2019.develops.stud11417025.smi_mnc.Autentikasi.Autentikasi;
import uas2019.develops.stud11417025.smi_mnc.RegistrasiTicket.MainActivity;

/**
 * Created by test on 7/12/2019.
 */

public class SessionManager {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context context;
    int mode;

    private static final String pref_name = "Gabriel";

    private static final String is_login = "isLogin";
    public static final String email  = "email";
    public static final String name = "gabs";
    public static final String password = "test";
    public static final String id = "1";
    public SessionManager(Context context){
        this.context = context;
        pref = context.getSharedPreferences(pref_name, mode);
        editor = pref.edit();
    }



    public void createSession(String mail, String nama, String id_user, String pwd){
        editor.putBoolean(is_login, true);
        editor.putString(email, mail);
        editor.putString(name, nama);
        editor.putString(id, id_user);
        editor.putString(password, pwd);
        editor.commit();
    }

    public void checkLogin(){
        if (!this.is_login()){
            Intent i = new Intent(context, Autentikasi.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);
        }else {
            Intent i = new Intent(context, MainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);
        }
    }

    private boolean is_login() {
        return pref.getBoolean(is_login, false);
    }

    public void logout(){
        editor.clear();
        editor.commit();
        Intent i = new Intent(context, Autentikasi.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
    }

    public HashMap<String, String> getUserDetails(){
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(pref_name, pref.getString(pref_name, null));
        user.put(email, pref.getString(email, null));
        user.put(name, pref.getString(name, null));
        user.put(id, pref.getString(id, null));
        user.put(password, pref.getString(password, null));
        return user;
    }
}
