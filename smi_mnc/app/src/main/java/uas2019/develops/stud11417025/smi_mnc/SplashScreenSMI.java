package uas2019.develops.stud11417025.smi_mnc;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

public class SplashScreenSMI extends AppCompatActivity {
    TextView smi;
    SessionManager sessionManager;
    private static final int CAMERA_REQUEST_CODE = 777;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        if(ContextCompat.checkSelfPermission(SplashScreenSMI.this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(SplashScreenSMI.this, new String[]{
                    Manifest.permission.CAMERA}, CAMERA_REQUEST_CODE);
        }
        sessionManager = new SessionManager(this);
        setContentView(R.layout.activity_splash_screen_smi);
        smi = (TextView) findViewById(R.id.smi);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                sessionManager.checkLogin();
                finish();
            }
        }, 5000);
    }
}
