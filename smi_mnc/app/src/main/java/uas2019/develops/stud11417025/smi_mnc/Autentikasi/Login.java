package uas2019.develops.stud11417025.smi_mnc.Autentikasi;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.json.JSONObject;

import java.security.Key;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.spec.SecretKeySpec;

import uas2019.develops.stud11417025.smi_mnc.Connection;
import uas2019.develops.stud11417025.smi_mnc.JSONParser;
import uas2019.develops.stud11417025.smi_mnc.R;
import uas2019.develops.stud11417025.smi_mnc.RegistrasiTicket.MainActivity;
import uas2019.develops.stud11417025.smi_mnc.SessionManager;

public class Login extends Fragment {
    private static final String ALGORITHM = "AES";
    private static final String KEY = "1Hbfh667adfDEJ78";
    public EditText uname, pass;
    Button login;
    JSONParser parser = new JSONParser();
    private static final String  TAG_SUCCESS = "success";
    SessionManager session;
    public static String usernames;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_login, container, false);

        uname = (EditText) view.findViewById(R.id.username);
        session = new SessionManager(getContext());
        pass = (EditText) view.findViewById(R.id.password);
        login = (Button) view.findViewById(R.id.login);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(uname.getText().toString().equals("") || pass.getText().toString().equals("")){
                    Toast.makeText(getContext(), "Form Username and Password must be filled in", Toast.LENGTH_SHORT).show();
                }
                else{
                    new LoginProccess().execute();
//                    startActivity(new Intent(getActivity(), MainActivity.class));
//                    Toast.makeText(getContext(), "Username : "+usernames, Toast.LENGTH_SHORT).show();
                }
            }
        });

        return view;
    }
    private static Key generateKey() throws Exception
    {
        Key key = new SecretKeySpec(Login.KEY.getBytes(),Login.ALGORITHM);
        return key;
    }
    //Login Process
    class LoginProccess extends AsyncTask<String, String, String>{
        @Override
        protected String doInBackground(String... args) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            String success = null;
            try{
                String url = Connection.url+"/user/" + uname.getText().toString() + "/" + pass.getText().toString();
                JSONObject object = parser.makeHttpRequest(url, "GET", params);
                Log.e("data : ", String.valueOf(object));
                success = object.toString();
                System.out.println("data : "+success);
                if(success.equals("{\"responseCode\":\"404\",\"responseMessage\":\"Account not registered\"}")){
                  return success;
                }
                else{
                    startActivity(new Intent(getContext(), MainActivity.class));
                    usernames = object.getString("name");
                    String username = object.getString("username");
                    String name = object.getString("name");
                    String password = object.getString("password");
                    String id = object.getString("id");
                    session.createSession(username, name, id, password);
                }
            } catch (Throwable throwable) {
                Log.e("Error na : ",throwable.getMessage());
                throwable.printStackTrace();
            }


            return success;
        }

        @Override
        protected void onPostExecute(String success) {
            if(success.equals("{\"responseCode\":\"404\",\"responseMessage\":\"Account not registered\"}")){
                Toast.makeText(getContext(), "Register Your Account", Toast.LENGTH_SHORT).show();
            }
            else{
                Toast.makeText(getContext(), "Login Success, Welcome : "+usernames, Toast.LENGTH_SHORT).show();
            }
        }
    }

}
